import numpy as np
import pandas as pd

nmodes = 4
ranks = [16,32]
dcTTMNCols = 4
TTMNCols = 3
numTrials = 4
commVcompList = []
SVDvTTMlist = []

for alg in ['dcTTM', 'TTM']:
    size = (0, dcTTMNCols) if alg == 'dcTTM' else (0, TTMNCols)
    avgBreakDown = np.empty(size)
    for i in ranks:
        rowsWMaxTotal = []
        for j in range(0, numTrials):
            filename = "{0}_varyRanks_r{1}_{2}.csv".format(alg, i, j)
            df = pd.read_csv(filename, index_col=False)
            if j == 0:
                columns = df.columns.to_list()
            rowsWMaxTotal.append(df.iloc[df['Total'].idxmax()].to_numpy())
        mean = np.mean(rowsWMaxTotal, axis=0)
        mean = np.reshape(mean, (1, -1))
        avgBreakDown = np.append(avgBreakDown, mean, axis=0)
    rdf = pd.DataFrame(avgBreakDown, columns = columns)
    filename = "{0}_varyRanks_avgTime.csv".format(alg)
    rdf.to_csv(filename)