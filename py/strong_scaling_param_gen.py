# Generates two type of files: the jobscript and the parameter file
# Each file has 5 versions corresponding to increasing number of nodes
import numpy as np

I_dims = [256, 256, 256, 256]
R_dims = [32, 32, 32, 32]
nnodes = np.array([1,2,4,8,16])
ncores = np.multiply(nnodes,32)
grids = np.array([[1,1,2,16],
                  [1,1,4,16],
                  [1,1,8,16],
                  [1,1,16,16],
                  [1,2,16,16]])

resultsDirectory = "/gpfs/alpine/csc345/scratch/zitongli/Andes/RandTucker_Scaling"
TuckerMPIDriverDirectory = "/ccs/home/zitongli/RandTucker/build"


def createJobScript(nnodes, ncores, jobScriptFileName, parameterFileNames):
  with open(jobScriptFileName, 'w') as slurmFile:
    slurmFile.write("#!/bin/bash \n")
    slurmFile.write("#SBATCH -A CSC345 \n")
    slurmFile.write("#SBATCH -J strong_scaling \n")
    slurmFile.write("#SBATCH -N {0} \n".format(nnodes))
    slurmFile.write("#SBATCH -t 0:20:00 \n")
    # slurmFile.write("#SBATCH --mem=0 \n")
    slurmFile.write("#SBATCH -p batch \n")
    slurmFile.write("#SBATCH -o strong_scaling.%J.o \n")
    slurmFile.write("#SBATCH -e strong_scaling.%J.e \n")
    slurmFile.write("date \n")
    slurmFile.write("echo --------------- \n")
    for parameterFileName in parameterFileNames:
      slurmFile.write("echo --------------- \n")
      slurmFile.write("srun -n{0} -N{1} -c1 --cpu-bind=cores {2}/mpi/drivers/bin/rhosvd_rekron --parameter-file {3} \n".format(ncores, nnodes, TuckerMPIDriverDirectory, parameterFileName))
      slurmFile.write("echo --------------- \n")
    
def createParameterFile(grid, STHOSVD, fileName, ind):
  with open(fileName,'w') as file:
    file.write("Print options = true \n")
    file.write("Global dims = {0} {1} {2} {3} \n".format(I_dims[0],I_dims[1],I_dims[2],I_dims[3]))
    file.write("Grid dims = {0} {1} {2} {3} \n".format(grid[0],grid[1],grid[2],grid[3]))
    file.write("Estimate ranks = {0} {1} {2} {3} \n".format(R_dims[0],R_dims[1],R_dims[2],R_dims[3]))
    file.write("True ranks = {0} {1} {2} {3} \n".format(R_dims[0],R_dims[1],R_dims[2],R_dims[3]))
    if(STHOSVD):
      file.write("Use STHOSVD algorithm = true \n")
      file.write("STHOSVD Timing file = {0}/strongScaling_sthosvd_nn{1}_{2}.csv \n".format(resultsDirectory, nnodes[i], ind))
    else:
      file.write("Use STHOSVD algorithm = false \n")
      file.write("RHOSVD Timing file = {0}/strongScaling_rhosvd_nn{1}_{2}.csv \n".format(resultsDirectory, nnodes[i], ind))

for i in range(len(nnodes)):
  for j in range(4):
    jobScriptFileName = "strongScaling_nn{0}_{1}.slurm".format(nnodes[i], j)
    sthosvdParameterFileName = "{0}/strongScaling_sthosvd_param_nn{1}_{2}.txt".format(resultsDirectory, nnodes[i], j)
    rhosvdParameterFileName = "{0}/strongScaling_rhosvd_param_nn{1}_{2}.txt".format(resultsDirectory, nnodes[i], j)

    createParameterFile(grids[i,:], True, sthosvdParameterFileName, j)
    createParameterFile(grids[i,:], False, rhosvdParameterFileName, j)
    a = np.random.uniform()
    if(j > 1):
      createJobScript(nnodes[i], ncores[i], jobScriptFileName, [sthosvdParameterFileName, rhosvdParameterFileName])
    else:
      createJobScript(nnodes[i], ncores[i], jobScriptFileName, [rhosvdParameterFileName, sthosvdParameterFileName])
