import numpy as np
import pandas as pd

nmodes = 4
sthosvdNCols = 57
rhosvdNCols = 26
I_dims = [256, 256, 256, 256]
nnodes = 8
ncores = 8*32
R_dims = np.array([[16, 16, 16, 16],
                  [32, 32, 32, 32],
                  [64, 64, 64, 64],
                  [128, 128, 128, 128],
                  ])
grid = [1,1,16,16]
commVcompList = []
SVDvTTMlist = []

for alg in ['sthosvd', 'rhosvd','truncateCore']:
  size = (0, sthosvdNCols) if alg == 'sthosvd' else (0, rhosvdNCols)
  avgBreakDown = np.empty(size)
  for i in nnodes:
    rowsWMaxTotal = []
    for j in range(0, numTrials):
        if alg == "truncateCore":
          filename = "varyRanks_rhosvd_r{1}_{2}_truncateCore.csv".format(i, j)
        else:
          filename = "varyRanks_{0}_r{1}_{2}.csv".format(alg, i, j)
        df = pd.read_csv(filename, index_col=False)
        if j == 0:
            columns = df.columns.to_list()
        rowsWMaxTotal.append(df.iloc[df['Total'].idxmax()].to_numpy())
    mean = np.mean(rowsWMaxTotal, axis=0)
    mean = np.reshape(mean, (1, -1))
    avgBreakDown = np.append(avgBreakDown, mean, axis=0)
  rdf = pd.DataFrame(avgBreakDown, columns = columns)

  M = np.zeros((len(nnodes), 2))
  for i in range(len(nnodes)):
    comm = 0
    comp = 0
    for j in range(nmodes):
        if alg == 'rhosvd':
          comm += rdf['dcttm_comm({0})'.format(j)][i]
          comp += rdf['dcttm_matmul({0})'.format(j)][i]
          comp += rdf['qr({0})'.format(j)][i]
        else:
          comm += rdf['Gram all-reduce({0})'.format(j)][i]
          comm += rdf['Gram all-gather({0})'.format(j)][i]
          comm += rdf['Gram all-to-all({0})'.format(j)][i]
          comp += rdf['Gram local multiply({0})'.format(j)][i]
          comp += rdf['Eigensolve({0})'.format(j)][i]
    M[i,0] = comm
    M[i,1] = comp
  commVcompList.append(M)

    # compare SVD vs TTM time
    M = np.zeros((len(nnodes), 2))
    for i in range(len(nnodes)):
        svdTime = 0
        ttmTime = 0
        for j in range(nmodes):
            if alg == 'rhosvd':
                svdTime += rdf['dcttm_total({0})'.format(j)][i]
                svdTime += rdf['qr({0})'.format(j)][i]
                ttmTime += rdf['ttm({0})'.format(j)][i]
            else:
                svdTime += rdf['Gram({0})'.format(j)][i]
                svdTime += rdf['Eigensolve({0})'.format(j)][i]
                ttmTime += rdf['TTM({0})'.format(j)][i]
        M[i,0] = svdTime
        M[i,1] = ttmTime
    SVDvTTMlist.append(M)

SVDvTTM = np.concatenate(SVDvTTMlist, axis=1)
commVcomp = np.concatenate(commVcompList, axis=1)
SVDvTTM = pd.DataFrame(SVDvTTM, columns = ['sthosvd_SVD', 'sthosvd_TTM',  'rhosvd_SVD', 'rhosvd_TTM'], index = nnodes)
SVDvTTM.to_csv("SVDvTTM.csv")
commVcomp = pd.DataFrame(commVcomp, columns = ['sthosvd_comm', 'sthosvd_comp',  'rhosvd_comm', 'rhosvd_comp'], index = nnodes)
commVcomp.to_csv("commVcomp_SVD.csv")