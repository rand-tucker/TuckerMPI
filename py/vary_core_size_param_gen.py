# Generates two type of files: the jobscript and the parameter file
# Each file has 5 versions corresponding to increasing number of nodes
import numpy as np

I_dims = [256, 256, 256, 256]
nnodes = 8
ncores = 8*32
R_dims = np.array([[16, 16, 16, 16],
                  [32, 32, 32, 32],
                  [64, 64, 64, 64],
                  [128, 128, 128, 128],
                  ])
grid = [1,1,16,16]

resultsDirectory = "/gpfs/alpine/csc345/scratch/zitongli/Andes/RandTucker_VaryRanks"
TuckerMPIDriverDirectory = "/ccs/home/zitongli/RandTucker/build"


def createJobScript(nnodes, ncores, jobScriptFileName, parameterFileNames):
  with open(jobScriptFileName, 'w') as slurmFile:
    slurmFile.write("#!/bin/bash \n")
    slurmFile.write("#SBATCH -A CSC345 \n")
    slurmFile.write("#SBATCH -J vary_ranks \n")
    slurmFile.write("#SBATCH -N {0} \n".format(nnodes))
    slurmFile.write("#SBATCH -t 0:20:00 \n")
    # slurmFile.write("#SBATCH --mem=0 \n")
    slurmFile.write("#SBATCH -p batch \n")
    slurmFile.write("#SBATCH -o vary_ranks.%J.o \n")
    slurmFile.write("#SBATCH -e vary_ranks.%J.e \n")
    slurmFile.write("date \n")
    slurmFile.write("echo --------------- \n")
    for parameterFileName in parameterFileNames:
      slurmFile.write("echo --------------- \n")
      slurmFile.write("srun -n{0} -N{1} -c1 --cpu-bind=cores {2}/mpi/drivers/bin/rhosvd_rekron --parameter-file {3} \n".format(ncores, nnodes, TuckerMPIDriverDirectory, parameterFileName))
      slurmFile.write("echo --------------- \n")
    
def createParameterFile(R_dims, STHOSVD, fileName, ind):
  with open(fileName,'w') as file:
    file.write("Print options = true \n")
    file.write("Global dims = {0} {1} {2} {3} \n".format(I_dims[0],I_dims[1],I_dims[2],I_dims[3]))
    file.write("Grid dims = {0} {1} {2} {3} \n".format(grid[0],grid[1],grid[2],grid[3]))
    file.write("Estimate ranks = {0} {1} {2} {3} \n".format(R_dims[0],R_dims[1],R_dims[2],R_dims[3]))
    file.write("True ranks = {0} {1} {2} {3} \n".format(R_dims[0],R_dims[1],R_dims[2],R_dims[3]))
    if(STHOSVD):
      file.write("Use STHOSVD algorithm = true \n")
      file.write("STHOSVD Timing file = {0}/varyRanks_sthosvd_r{1}_{2}.csv \n".format(resultsDirectory, R_dims[0], ind))
    else:
      file.write("Use STHOSVD algorithm = false \n")
      file.write("STHOSVD Timing file = {0}/varyRanks_rhosvd_r{1}_{2}_truncateCore.csv \n".format(resultsDirectory, R_dims[0], ind))
      file.write("RHOSVD Timing file = {0}/varyRanks_rhosvd_r{1}_{2}.csv \n".format(resultsDirectory, R_dims[0], ind))

    

for i in range(len(R_dims)):
  for j in range(4):
    jobScriptFileName = "varyRanks_r{0}_{1}.slurm".format(R_dims[i,0], j)
    sthosvdParameterFileName = "{0}/varyRanks_sthosvd_param_r{1}_{2}.txt".format(resultsDirectory, R_dims[i,0], j)
    rhosvdParameterFileName = "{0}/varyRanks_rhosvd_param_r{1}_{2}.txt".format(resultsDirectory, R_dims[i,0], j)

    createParameterFile(R_dims[i,:], True, sthosvdParameterFileName, j)
    createParameterFile(R_dims[i,:], False, rhosvdParameterFileName, j)
    a = np.random.uniform()
    if(j > 1):
      createJobScript(nnodes, ncores, jobScriptFileName, [sthosvdParameterFileName, rhosvdParameterFileName])
    else:
      createJobScript(nnodes, ncores, jobScriptFileName, [rhosvdParameterFileName, sthosvdParameterFileName])
