[![build status](https://gitlab.com/tensors/TuckerMPI/badges/master/build.svg)](https://gitlab.com/tensors/TuckerMPI/commits/master)
[![codecov](https://codecov.io/gl/tensors/tuckermpi/branch/master/graph/badge.svg?token=FO5DT6r7wc)](https://codecov.io/gl/tensors/tuckermpi)
<a href="https://scan.coverity.com/projects/tuckermpi">
  <img alt="Coverity Scan Build Status"
       src="https://scan.coverity.com/projects/10762/badge.svg"/>
</a>

This is the GIT repo for the work on building a parallel Tucker for combustion data.                                                   

For more information:  
[Zitong Li](mailto:zitongl5@uci.edu)  
[Grey Ballard](mailto:ballard@wfu.edu)

WARNING
-------
This code is still in development, but we welcome evaluation by friendly expert users.  Please contact us if you have any questions, or submit an issue if you find a bug or wish to request a new feature.

Requirements
------------
MPI implementation (We use openMPI MPICH2, and MVAPICH2)  
BLAS implementation  
LAPACK implementation  
C++11 or greater  

Documentation
-------------
Please see https://tensors.gitlab.io/TuckerMPI

Papers
------
Parallel Randomized Tucker Decomposition Algorithms  
Rachel Minster, Zitong Li, and Grey Ballard  
[Find it on arXiv!](https://arxiv.org/abs/1905.07311)  