#include<random>
#include<chrono>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <algorithm>
#include "assert.h"
#include "TuckerMPI.hpp"
#include "Tucker.hpp"
#include "Tucker_IO_Util.hpp"
#include "TuckerMPI_IO_Util.hpp"
//This driver is purely for performance benchmarking purposes. Use this to compare the performance between the dcTTM and reduce scatter TTM.
int main(int argc, char* argv[])
{
  #ifdef DRIVER_SINGLE
    using scalar_t = float;
  #else
    using scalar_t = double;
  #endif  // specify precision
  //
  // Initialize MPI
  //
  MPI_Init(&argc, &argv);

  //
  // Get the rank of this MPI process
  // Only rank 0 will print to stdout
  //
  int rank, nprocs;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&nprocs);

  std::string paramfn                   = Tucker::parseString(argc, (const char**)argv, "--parameter-file", "paramfile.txt");
  std::vector<std::string> fileAsString = Tucker::getFileAsStrings(paramfn);

  unsigned int seed                     = Tucker::stringParse<unsigned int>(fileAsString, "seed", 123);
  Tucker::SizeArray* proc_grid_dims     = Tucker::stringParseSizeArray(fileAsString, "Processor grid dims");
  Tucker::SizeArray* I_dims             = Tucker::stringParseSizeArray(fileAsString, "Global dims");
  Tucker::SizeArray* R_dims             = Tucker::stringParseSizeArray(fileAsString, "Factor matrices sizes");
  std::string resultsDirectory          = Tucker::stringParse<std::string>(fileAsString, "Path to output files", "");
  std::string resultsFilePrefix         = Tucker::stringParse<std::string>(fileAsString, "Results file prefix", "");
  int numTrials                         = Tucker::stringParse<int>(fileAsString, "Number of runs", 1);
  
  if(I_dims->size() != R_dims->size()){
    std::cerr << "Error: The size of the ranks array (" << R_dims->size() <<") should be the same as the size of global dims array. \n";
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Abort(MPI_COMM_WORLD, 1);
  }

  ///////////////////////////////////////////////////////////////
  // Generate the seeds for each MPI process and scatter them  //
  ///////////////////////////////////////////////////////////////
  int myseed;
  if(rank == 0) {
    unsigned* seeds = Tucker::MemoryManager::safe_new_array<unsigned>(nprocs);

    srand(seed);
    for(int i=0; i<nprocs; i++) {
      seeds[i] = rand();
    }

    MPI_Scatter(seeds,1,MPI_INT,&myseed,1,MPI_INT,0,MPI_COMM_WORLD);

    Tucker::MemoryManager::safe_delete_array<unsigned>(seeds,nprocs);
  }
  else {
    MPI_Scatter(NULL,1,MPI_INT,&myseed,1,MPI_INT,0,MPI_COMM_WORLD);
  }


  //////////////////////////////////////////////
  // Create the normal distribution generator //
  //////////////////////////////////////////////
  std::default_random_engine generator(myseed);
  std::normal_distribution<scalar_t> distribution;

  ///////////////////////////////////////////////
  // Set up distribution object for the tensor //
  ///////////////////////////////////////////////
  TuckerMPI::Distribution* dist = Tucker::MemoryManager::safe_new<TuckerMPI::Distribution>(*I_dims, *proc_grid_dims);

  //////////////////////////////
  // Generate a random tensor //
  //////////////////////////////
  TuckerMPI::Tensor<scalar_t>* X = Tucker::MemoryManager::safe_new<TuckerMPI::Tensor<scalar_t>>(dist);
  size_t nnz = dist->getLocalDims().prod();
  scalar_t* dataptr = X->getLocalTensor()->data();
  for(size_t i=0; i<nnz; i++) {
    dataptr[i] = distribution(generator);
  }

  //////////////////////////////////////////////////////////////////
  // Create the factor matrices on process 0, then broadcast them //
  //////////////////////////////////////////////////////////////////
  int nd = I_dims->size();
  Tucker::Matrix<scalar_t>** Ulist = Tucker::MemoryManager::safe_new_array<Tucker::Matrix<scalar_t>*>(nd);
  for(int d=0; d<nd; d++) {
    if(rank == 0) std::cout << "Generating factor matrix " << d << "...\n";
    int nrows = (*R_dims)[d];
    int ncols = (*I_dims)[d];
    Ulist[d] = Tucker::MemoryManager::safe_new<Tucker::Matrix<scalar_t>>(nrows,ncols);
    nnz = nrows*ncols;
    dataptr = Ulist[d]->data();
    if(rank == 0) {
      for(size_t i=0; i<nnz; i++) {
        dataptr[i] = distribution(generator);
      }
    }
    TuckerMPI::MPI_Bcast_(dataptr,nnz,0,MPI_COMM_WORLD);
  }
  size_t max_lcl_nnz_x = 1;
  for(int i=0; i<nd; i++) {
    max_lcl_nnz_x *= X->getDistribution()->getMap(i,false)->getMaxNumEntries();
  }

  int ntimers;
  std::string timing_file;
  double* gathered_data;
  double* raw_array;
  for(int i=0; i<numTrials; i++){
    ////////////////
    // dcTTM test //
    ////////////////
    timing_file = resultsDirectory + "/"+ resultsFilePrefix + "_dcttm_run" + std::to_string(i) + "_.csv"; 
    ntimers = 7; //number of timers
    Tucker::Timer* dcttm_total_timer = Tucker::MemoryManager::safe_new<Tucker::Timer>();
    Tucker::Timer* dcttm_mult_timer = Tucker::MemoryManager::safe_new<Tucker::Timer>();
    Tucker::Timer* dcttm_all_reduce_timer = Tucker::MemoryManager::safe_new<Tucker::Timer>();
    Tucker::Timer* dcttm_newSize_timer = Tucker::MemoryManager::safe_new<Tucker::Timer>();
    Tucker::Timer* dcttm_dist_timer = Tucker::MemoryManager::safe_new<Tucker::Timer>();
    Tucker::Timer* dcttm_mem_timer = Tucker::MemoryManager::safe_new<Tucker::Timer>();
    Tucker::Timer* dcttm_creatMaps_timer = Tucker::MemoryManager::safe_new<Tucker::Timer>();
  
    MPI_Barrier(MPI_COMM_WORLD);
    dcttm_total_timer->start();
    Tucker::Tensor<scalar_t>* dcTTMProduct = TuckerMPI::dcttm(X, Ulist, dcttm_mult_timer, dcttm_all_reduce_timer, dcttm_newSize_timer, dcttm_dist_timer, dcttm_creatMaps_timer, dcttm_mem_timer);
    dcttm_total_timer->stop();
    Tucker::MemoryManager::safe_delete(dcTTMProduct);

    // Print time //
    raw_array = Tucker::MemoryManager::safe_new_array<double>(ntimers);
    raw_array[0] = dcttm_mult_timer->duration();
    raw_array[1] = dcttm_all_reduce_timer->duration();
    raw_array[2] = dcttm_newSize_timer->duration();
    raw_array[3] = dcttm_dist_timer->duration();
    raw_array[4] = dcttm_creatMaps_timer->duration();
    raw_array[5] = dcttm_mem_timer->duration();
    raw_array[6] = dcttm_total_timer->duration();
    // Allocate memory on process 0
    
    if(rank == 0) {
      gathered_data = Tucker::MemoryManager::safe_new_array<double>(ntimers*nprocs);
    }
    else {
      gathered_data = 0;
    }
    // Gather all the data to process 0
    MPI_Gather((void*)raw_array, ntimers, MPI_DOUBLE, (void*)gathered_data, ntimers, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    Tucker::MemoryManager::safe_delete_array(raw_array,ntimers);

    if(rank ==0){
      std::ofstream os(timing_file);
      if(!os.is_open()){
        std::cerr<<"Failed to open file : "<<std::endl;
        MPI_Abort(MPI_COMM_WORLD, 1);
      }
      // Create the header row
      os << "computation,"
      << "communication,"
      << "newSize,"
      << "dist,"
      << "createMaps,"
      << "mem,"
      << "total\n";
    
      // For each MPI process
      for(int r=0; r<nprocs; r++) {
        // For each timer belonging to that process
        for(int t=0; t<ntimers; t++) {
          os << gathered_data[r*ntimers+t] << ",";
        }
        os << std::endl;
      }
      os.close();  
      Tucker::MemoryManager::safe_delete_array(gathered_data, ntimers*nprocs);
    }
    Tucker::MemoryManager::safe_delete(dcttm_total_timer);
    Tucker::MemoryManager::safe_delete(dcttm_mult_timer);
    Tucker::MemoryManager::safe_delete(dcttm_all_reduce_timer);
    Tucker::MemoryManager::safe_delete(dcttm_newSize_timer);
    Tucker::MemoryManager::safe_delete(dcttm_dist_timer);
    Tucker::MemoryManager::safe_delete(dcttm_mem_timer);
    Tucker::MemoryManager::safe_delete(dcttm_creatMaps_timer); 

    //////////////
    // TTM test //
    //////////////
    timing_file = resultsDirectory + "/"+ resultsFilePrefix + "_ttm_run" + std::to_string(i) + "_.csv"; 
    Tucker::Timer* ttm_total_timer = Tucker::MemoryManager::safe_new<Tucker::Timer>();
    Tucker::Timer* ttm_mult_timer = Tucker::MemoryManager::safe_new<Tucker::Timer>();
    Tucker::Timer* ttm_pack_timer = Tucker::MemoryManager::safe_new<Tucker::Timer>();
    Tucker::Timer* ttm_dist_timer = Tucker::MemoryManager::safe_new<Tucker::Timer>();
    Tucker::Timer* ttm_mem_timer = Tucker::MemoryManager::safe_new<Tucker::Timer>();
    Tucker::Timer* ttm_reduce_scatter_timer = Tucker::MemoryManager::safe_new<Tucker::Timer>();
    Tucker::Timer* ttm_reduce_timer = Tucker::MemoryManager::safe_new<Tucker::Timer>();
  
    //realign timing to prevent some processors from starting late.
    MPI_Barrier(MPI_COMM_WORLD);
    ttm_total_timer->start();
    TuckerMPI::Tensor<scalar_t>* oldTTMProduct = X;
    for(int d=0; d<nd; d++) {
      TuckerMPI::Tensor<scalar_t>* temp = TuckerMPI::ttm(oldTTMProduct, d, Ulist[d], false, ttm_mult_timer, ttm_pack_timer, ttm_reduce_scatter_timer, ttm_reduce_timer, ttm_dist_timer, ttm_mem_timer, max_lcl_nnz_x);
      if(oldTTMProduct != X) {
        Tucker::MemoryManager::safe_delete<TuckerMPI::Tensor<scalar_t>>(oldTTMProduct);
      }
      oldTTMProduct = temp;
    }
    ttm_total_timer->stop();
    Tucker::MemoryManager::safe_delete(oldTTMProduct);

    ////////////////
    // Print time //
    ////////////////
    const int ntimers = 6; //number of timers shared by all modes
    raw_array = Tucker::MemoryManager::safe_new_array<double>(ntimers);
    raw_array[0] = ttm_mult_timer->duration();
    raw_array[1] = ttm_reduce_scatter_timer->duration() + ttm_reduce_timer->duration();
    raw_array[2] = ttm_pack_timer->duration();
    raw_array[3] = ttm_dist_timer->duration();
    raw_array[4] = ttm_mem_timer->duration();
    raw_array[5] = ttm_total_timer->duration();

    // Allocate memory on process 0
    if(rank == 0) {
      gathered_data = Tucker::MemoryManager::safe_new_array<double>(ntimers*nprocs);
      
    }
    else {
      gathered_data = 0;
    }
    // Gather all the data to process 0
    MPI_Gather((void*)raw_array, ntimers, MPI_DOUBLE, (void*)gathered_data, ntimers, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    Tucker::MemoryManager::safe_delete_array(raw_array,ntimers);

    if(rank ==0){
      std::ofstream os(timing_file);
      if(!os.is_open()){
        std::cerr<<"Failed to open file : "<<std::endl;
        MPI_Abort(MPI_COMM_WORLD, 1);
      }
      // Create the header row
      os << "computation," 
      << "communication,"
      << "pack,"
      << "dist,"
      << "mem,"
      << "total\n";

      for(int r=0; r<nprocs; r++) {
        for(int t=0; t<ntimers; t++) {
          os << gathered_data[r*ntimers+t] << ",";
        }
        os << std::endl;
      }
      os.close();
      Tucker::MemoryManager::safe_delete_array(gathered_data, ntimers*nprocs);
    }
    Tucker::MemoryManager::safe_delete(ttm_total_timer);
    Tucker::MemoryManager::safe_delete(ttm_mult_timer);
    Tucker::MemoryManager::safe_delete(ttm_pack_timer);
    Tucker::MemoryManager::safe_delete(ttm_dist_timer);
    Tucker::MemoryManager::safe_delete(ttm_mem_timer);
    Tucker::MemoryManager::safe_delete(ttm_reduce_scatter_timer);
    Tucker::MemoryManager::safe_delete(ttm_reduce_timer);
  }

  MPI_Finalize();
  return EXIT_SUCCESS;
}
