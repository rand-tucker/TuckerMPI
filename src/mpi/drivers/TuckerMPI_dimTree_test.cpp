
#include "TuckerMPI.hpp"
#include "Tucker.hpp"
#include "Tucker_IO_Util.hpp"
#include "TuckerMPI_IO_Util.hpp"
#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <random>
#include <chrono>
#include "assert.h"


int main(int argc, char* argv[])
{
  #ifdef DRIVER_SINGLE
    using scalar_t = float;
  #else
    using scalar_t = double;
  #endif  // specify precision

  //
  // Initialize MPI
  //
  MPI_Init(&argc, &argv);

  //
  // Get the rank of this MPI process
  // Only rank 0 will print to stdout
  //
  int rank, nprocs;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&nprocs);
  //
  // Get the name of the input file
  //
  std::string paramfn = Tucker::parseString(argc, (const char**)argv,
      "--parameter-file", "paramfile.txt");
  
  //
  // Parse parameter file
  // Put's each line as a string into a vector ignoring empty lines
  // and comments
  //
  std::vector<std::string> fileAsString = Tucker::getFileAsStrings(paramfn);

  Tucker::SizeArray* I_dims             = Tucker::stringParseSizeArray(fileAsString, "Global dims");
  Tucker::SizeArray* R_dims             = Tucker::stringParseSizeArray(fileAsString, "Estimated ranks");
  Tucker::SizeArray* trueRanks          = Tucker::stringParseSizeArray(fileAsString, "True ranks");
  Tucker::SizeArray* proc_grid_dims     = Tucker::stringParseSizeArray(fileAsString, "Grid dims");

  unsigned int seed                     = Tucker::stringParse<unsigned int>(fileAsString, "RNG seed", std::chrono::system_clock::now().time_since_epoch().count());
  int p                                 = Tucker::stringParse<int>(fileAsString, "Oversampling Parameter", 5);
  scalar_t eps                          = Tucker::stringParse<scalar_t>(fileAsString, "Noise", 1e-4);

  bool boolPrintOptions                 = Tucker::stringParse<bool>(fileAsString, "Print options", false);
  bool boolReconstruct                  = Tucker::stringParse<bool>(fileAsString, "Reconstruct tensor", false);
  bool useDimTree                       = Tucker::stringParse<bool>(fileAsString, "Use dimTree", true);
  bool useSrhtMatrices                  = Tucker::stringParse<bool>(fileAsString, "Use SRHT matrices", true);
  std::string timing_file               = Tucker::stringParse<std::string>(fileAsString, "Timing file", "runtime.csv");
  // std::string rhosvd_dimTree_timing_file       = Tucker::stringParse<std::string>(fileAsString, "RHOSVD DimTree Timing file", "sthosvd_dimTree_runtime.csv");
  // std::string rhosvd_timing_file               = Tucker::stringParse<std::string>(fileAsString, "RHOSVD Timing file", "rhosvd_runtime.csv");
    
  int nd = I_dims->size();
  //
  // Print options
  //
  if (rank == 0 && boolPrintOptions) {
    std::cout << "The global dimensions of the tensor to be scaled or compressed\n";
    std::cout << "- Global dims = " << *I_dims << std::endl << std::endl;

    std::cout << "The global dimensions of the processor grid\n";
    std::cout << "- Grid dims = " << *proc_grid_dims << std::endl << std::endl;

    std::cout << "If true, print the parameters\n";
    std::cout << "- Print options = " << (boolPrintOptions ? "true" : "false") << std::endl << std::endl;


    // std::cout << "List of filenames of raw data to be read\n";
    // std::cout << "- Input file list = " << in_fns_file << std::endl << std::endl;

    std::cout << std::endl;
  }

  ///////////////////////
  // Check array sizes //
  ///////////////////////
  // Does |grid| == nprocs?
  if ((int)proc_grid_dims->prod() != nprocs){
    if (rank==0) {
      std::cerr << "Processor grid dimensions do not multiply to nprocs" << std::endl;
      std::cout << "Processor grid dimensions: " << *proc_grid_dims << std::endl;
    }
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Abort(MPI_COMM_WORLD, 1);
  }
  if (nd != proc_grid_dims->size()) {
    if (rank == 0) {
      std::cerr << "Error: The size of global dimension array (" << nd;
      std::cerr << ") must be equal to the size of the processor grid ("
          << proc_grid_dims->size() << ")" << std::endl;
    }
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Abort(MPI_COMM_WORLD, 1);
  }

  ///////////////////////////////////////////////////////////////
  // Generate the seeds for each MPI process and scatter them  //
  ///////////////////////////////////////////////////////////////
  int myseed;
  if(rank == 0) {
    unsigned* seeds = Tucker::MemoryManager::safe_new_array<unsigned>(nprocs);

    srand(seed);
    for(int i=0; i<nprocs; i++) {
      seeds[i] = rand();
    }

    MPI_Scatter(seeds,1,MPI_INT,&myseed,1,MPI_INT,0,MPI_COMM_WORLD);

    Tucker::MemoryManager::safe_delete_array<unsigned>(seeds,nprocs);
  }
  else {
    MPI_Scatter(NULL,1,MPI_INT,&myseed,1,MPI_INT,0,MPI_COMM_WORLD);
  }

  //////////////////////////////////////////////
  // Create the normal distribution generator //
  //////////////////////////////////////////////
  std::default_random_engine generator(myseed);
  std::normal_distribution<scalar_t> distribution;

  /////////////////////////////////////////////
  // Set up distribution object for the core //
  /////////////////////////////////////////////
  TuckerMPI::Distribution* core_dist =
      Tucker::MemoryManager::safe_new<TuckerMPI::Distribution>(*trueRanks, *proc_grid_dims);

  ///////////////////////////////////
  // Generate a random core tensor //
  ///////////////////////////////////
  if(rank == 0) std::cout << "Generating a random core tensor...\n";
  TuckerMPI::TuckerTensor<scalar_t> fact(nd);
  fact.G = Tucker::MemoryManager::safe_new<TuckerMPI::Tensor<scalar_t>>(core_dist);
  size_t nnz = core_dist->getLocalDims().prod();
  scalar_t* dataptr = fact.G->getLocalTensor()->data();
  for(size_t i=0; i<nnz; i++) {
    dataptr[i] = distribution(generator);
  }

  //////////////////////////////////////////////////////////////////
  // Create the factor matrices on process 0, then broadcast them //
  // This could tecnically be a single communication              //
  //////////////////////////////////////////////////////////////////
  for(int d=0; d<nd; d++) {
    int nrows = (*I_dims)[d];
    int ncols = (*trueRanks)[d];
    fact.U[d] = Tucker::MemoryManager::safe_new<Tucker::Matrix<scalar_t>>(nrows,ncols);
    nnz = nrows*ncols;
    dataptr = fact.U[d]->data();
    if(rank == 0) {
      for(size_t i=0; i<nnz; i++) {
        dataptr[i] = distribution(generator);
      }
    }
    TuckerMPI::MPI_Bcast_(dataptr,nnz,0,MPI_COMM_WORLD);
  }

  ////////////////////////////////////////////////////////
  // Construct the global tensor using a series of TTMs //
  ////////////////////////////////////////////////////////
  TuckerMPI::Tensor<scalar_t>* X = fact.G;
  for(int d=0; d<nd; d++) {
    TuckerMPI::Tensor<scalar_t>* temp = TuckerMPI::ttm(X, d, fact.U[d]);
    if(X != fact.G) {
      Tucker::MemoryManager::safe_delete(X);
    }
    X = temp;
  }

  
  Tucker::SizeArray* temp;
  if(useDimTree){
    const TuckerMPI::TuckerTensor<scalar_t>* solutionWithDimTree;
    if(useSrhtMatrices)
      solutionWithDimTree = TuckerMPI::RHOSVD(X, 123, R_dims, temp, p, true, true);
    else
      solutionWithDimTree = TuckerMPI::RHOSVD(X, 123, R_dims, temp, p, true, false);
    solutionWithDimTree->printTimersRHOSVDDimTree(timing_file,true);
  }
  else{
    const TuckerMPI::TuckerTensor<scalar_t>* solutionWithoutDimTree;
    if(useSrhtMatrices)
      solutionWithoutDimTree = TuckerMPI::RHOSVD(X, 123, R_dims, temp, p, false, true);
    else
      solutionWithoutDimTree = TuckerMPI::RHOSVD(X, 123, R_dims, temp, p, false, false);
    solutionWithoutDimTree->printTimersRHOSVD(timing_file, true);
  }

  // /////////////////////////
  // // Accuracy Comparison //
  // /////////////////////////
  // if(boolReconstruct){
  //   scalar_t normX = X->norm2();
  //   TuckerMPI::Tensor<scalar_t>* rhosvdReconT = solutionWithoutDimTree->reconstructTensor();
  //   TuckerMPI::Tensor<scalar_t>* rhosvdDiff = X->subtract(rhosvdReconT);
  //   scalar_t normRhosvdDiff = rhosvdDiff->norm2();
  //   TuckerMPI::Tensor<scalar_t>* dimTreeReconT = solutionWithDimTree->reconstructTensor();
  //   TuckerMPI::Tensor<scalar_t>* dimTreeDiff = X->subtract(dimTreeReconT);
  //   scalar_t normDimTreeRHOSVDDiff = dimTreeDiff->norm2();
  //   if(rank==0){
  //     std::cout << "normX squared: " << normX << std::endl;
  //     std::cout << "RHOSVD absolute error: " << normRhosvdDiff << std::endl;
  //     std::cout << "RHOSVD with DimTree absolute error: " << normDimTreeRHOSVDDiff << std::endl;
  //     std::cout << "RHOSVD Relative error: " << std::sqrt(normRhosvdDiff/normX) << std::endl;
  //     std::cout << "RHOSVD with DimTree Relative error: " << std::sqrt(normDimTreeRHOSVDDiff/normX) << std::endl;
  //   }
  // }

  MPI_Finalize();
  return EXIT_SUCCESS;
}