// similar to TuckerMPI_rhosvd_rekron.cpp but can run multiple trials without reloading the input. 
#include "TuckerMPI.hpp"
#include "Tucker.hpp"
#include "Tucker_IO_Util.hpp"
#include "TuckerMPI_IO_Util.hpp"
#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <random>
#include <chrono>
#include "assert.h"

int main(int argc, char* argv[])
{
  #ifdef DRIVER_SINGLE
    using scalar_t = float;
  #else
    using scalar_t = double;
  #endif  // specify precision

  //
  // Initialize MPI
  //
  MPI_Init(&argc, &argv);

  //
  // Get the rank of this MPI process
  // Only rank 0 will print to stdout
  //
  int rank, nprocs;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  MPI_Comm_size(MPI_COMM_WORLD,&nprocs);

  //
  // Get the name of the input file
  //
  std::string paramfn = Tucker::parseString(argc, (const char**)argv,
      "--parameter-file", "paramfile.txt");

  //
  // Parse parameter file
  // Put's each line as a string into a vector ignoring empty lines
  // and comments
  //
  std::vector<std::string> fileAsString = Tucker::getFileAsStrings(paramfn);

  Tucker::SizeArray* I_dims             = Tucker::stringParseSizeArray(fileAsString, "Global dims");
  Tucker::SizeArray* proc_grid_dims     = Tucker::stringParseSizeArray(fileAsString, "Grid dims");
  Tucker::SizeArray* R_dims             = Tucker::stringParseSizeArray(fileAsString, "Estimated ranks");
  Tucker::SizeArray* subRanks           = Tucker::stringParseSizeArray(fileAsString, "Subranks");

  bool boolPrintOptions                 = Tucker::stringParse<bool>(fileAsString, "Print options", false);
  bool computeNorm                      = Tucker::stringParse<bool>(fileAsString, "Compute tensor norm", false);
  bool truncateCoreInParallel           = Tucker::stringParse<bool>(fileAsString, "Truncate core in parallel", true);

  int oversampling                      = Tucker::stringParse<int>(fileAsString, "Oversampling parameter", 5);
  int numTrials                         = Tucker::stringParse<int>(fileAsString, "Number of trials", 5);

  bool useSrhtMatrices                  = Tucker::stringParse<bool>(fileAsString, "Use SRHT matrices", true);

  bool STHOSVD                          = Tucker::stringParse<bool>(fileAsString, "Use STHOSVD", false);
  bool RSTHOSVD_dense_Gaussian          = Tucker::stringParse<bool>(fileAsString, "Use RSTHOSVD_dense_Gaussian", false);
  bool RSTHOSVD_kron_ttm                = Tucker::stringParse<bool>(fileAsString, "Use RSTHOSVD_kron_ttm", false);
  bool RSTHOSVD_kron_dcttm              = Tucker::stringParse<bool>(fileAsString, "Use RSTHOSVD_kron_dcttm", false);
  bool RHOSVD_ttm_noReuse               = Tucker::stringParse<bool>(fileAsString, "Use RHOSVD_ttm_noReuse", false);
  bool RHOSVD_dcttm_noReuse             = Tucker::stringParse<bool>(fileAsString, "Use RHOSVD_dcttm_noReuse", false);
  bool RHOSVD_dcttm_reuse_noDimTree     = Tucker::stringParse<bool>(fileAsString, "Use RHOSVD_dcttm_reuse_noDimTree", false);
  bool RHOSVD_dcttm_reuse_dimTree       = Tucker::stringParse<bool>(fileAsString, "Use RHOSVD_dcttm_reuse_dimTree", false);

  bool generateRandomInputTensor        = Tucker::stringParse<bool>(fileAsString, "Generate random input tensor", true);
  Tucker::SizeArray* trueRanks = 0;
  if(generateRandomInputTensor)
    trueRanks                           = Tucker::stringParseSizeArray(fileAsString, "True ranks");
  unsigned int seed                     = Tucker::stringParse<unsigned int>(fileAsString, "RNG seed", std::chrono::system_clock::now().time_since_epoch().count());
  bool addNoise                         = Tucker::stringParse<bool>(fileAsString, "Add noise", true);
  scalar_t eps                          = Tucker::stringParse<scalar_t>(fileAsString, "Noise", 1e-4);

  bool boolWriteResults                 = Tucker::stringParse<bool>(fileAsString, "Write core tensor and factor matrices", false);
  std::string output_directory          = Tucker::stringParse<std::string>(fileAsString, "Results directory", "results");
  std::string input_file                = Tucker::stringParse<std::string>(fileAsString, "Input file names", "filenames.txt");
  std::string subRanks_file             = Tucker::stringParse<std::string>(fileAsString, "Subranks file", "subranks.txt");
    
  int nd = I_dims->size();

  //
  // Print options
  //
  if (rank == 0 && boolPrintOptions) {
    std::cout << "The global dimensions of the tensor to be scaled or compressed\n";
    std::cout << "- Global dims = " << *I_dims << std::endl << std::endl;

    std::cout << "The global dimensions of the processor grid\n";
    std::cout << "- Grid dims = " << *proc_grid_dims << std::endl << std::endl;

    std::cout << "Estimated size of the core tensor\n";
    std::cout << "- Estimated ranks = " << *R_dims << std::endl << std::endl;

    std::cout << "Predefined subranks\n";
    std::cout << "- subranks = " << *subRanks << std::endl << std::endl;

    if(trueRanks){
      std::cout << "Predefined true ranks of the synthetic input tensor. It will not be used when reading input from file.\n";
      std::cout << "- trueRanks = " << *trueRanks << std::endl << std::endl;
    }
    std::cout << "If true, compute the norm of the input tensor and the relative error of compression: |core| - |X|. \n"
    std::cout << "- Compute tensor norm" << computeNorm << std::endl << std::endl;

    std::cout << "If true, record the core tensor and all factors\n";
    std::cout << "- Write core tensor and all factors = " << (boolWriteResults ? "true" : "false") << std::endl << std::endl;

    std::cout << "If true and using one of the randomized algorithms, make the final truncation of the sketch in parallel. \n";
    std::cout << "- Truncate core in parallel = " << (truncateCoreInParallel ? "true" : "false") << std::endl << std::endl;

    std::cout << "If true, generate a random input tensor. \n" 
    std::cout << "- Generate random input tensor = " << generateRandomInputTensor << std::endl << std::endl;

    std::cout << "Directory location of core tensor and factor matrices\n";
    if(boolWriteResults) std::cout << "NOTE: Please ensure that this directory actually exists!\n";
    std::cout << "- Output directory = " << output_directory << std::endl << std::endl;
  }

  ///////////////////////
  // Check array sizes //
  ///////////////////////
  // Does |grid| == nprocs?
  if ((int)proc_grid_dims->prod() != nprocs){
    if (rank==0) {
      std::cerr << "Processor grid dimensions do not multiply to nprocs" << std::endl;
      std::cout << "Processor grid dimensions: " << *proc_grid_dims << std::endl;
    }
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Abort(MPI_COMM_WORLD, 1);
  }

  if (nd != proc_grid_dims->size()) {
    if (rank == 0) {
      std::cerr << "Error: The size of global dimension array (" << nd;
      std::cerr << ") must be equal to the size of the processor grid ("
          << proc_grid_dims->size() << ")" << std::endl;
    }
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Abort(MPI_COMM_WORLD, 1);
  }

  ///////////////////////////////
  // Set up distributed tensor //
  ///////////////////////////////
  TuckerMPI::Distribution* dist = Tucker::MemoryManager::safe_new<TuckerMPI::Distribution>(*I_dims, *proc_grid_dims);
  TuckerMPI::Tensor<scalar_t>* X = Tucker::MemoryManager::safe_new<TuckerMPI::Tensor<scalar_t>>(dist);

  if(generateRandomInputTensor){
    ///////////////////////////////////////////////////////////////
    // Generate the seeds for each MPI process and scatter them  //
    ///////////////////////////////////////////////////////////////
    int myseed;
    if(rank == 0) {
      unsigned* seeds = Tucker::MemoryManager::safe_new_array<unsigned>(nprocs);

      srand(seed);
      for(int i=0; i<nprocs; i++) {
        seeds[i] = rand();
      }

      MPI_Scatter(seeds,1,MPI_INT,&myseed,1,MPI_INT,0,MPI_COMM_WORLD);

      Tucker::MemoryManager::safe_delete_array<unsigned>(seeds,nprocs);
    }
    else {
      MPI_Scatter(NULL,1,MPI_INT,&myseed,1,MPI_INT,0,MPI_COMM_WORLD);
    }

    //////////////////////////////////////////////
    // Create the normal distribution generator //
    //////////////////////////////////////////////
    std::default_random_engine generator(myseed);
    std::normal_distribution<scalar_t> distribution;

    /////////////////////////////////////////////
    // Set up distribution object for the core //
    /////////////////////////////////////////////
    TuckerMPI::Distribution* core_dist =
        Tucker::MemoryManager::safe_new<TuckerMPI::Distribution>(*trueRanks, *proc_grid_dims);

    ///////////////////////////////////
    // Generate a random core tensor //
    ///////////////////////////////////
    if(rank == 0) std::cout << "Generating a random core tensor...\n";
    TuckerMPI::TuckerTensor<scalar_t> fact(nd);
    fact.G = Tucker::MemoryManager::safe_new<TuckerMPI::Tensor<scalar_t>>(core_dist);
    size_t nnz = core_dist->getLocalDims().prod();
    scalar_t* dataptr = fact.G->getLocalTensor()->data();
    for(size_t i=0; i<nnz; i++) {
      dataptr[i] = distribution(generator);
    }

    //////////////////////////////////////////////////////////////////
    // Create the factor matrices on process 0, then broadcast them //
    // This could tecnically be a single communication              //
    //////////////////////////////////////////////////////////////////
    for(int d=0; d<nd; d++) {
      int nrows = (*I_dims)[d];
      int ncols = (*trueRanks)[d];
      fact.U[d] = Tucker::MemoryManager::safe_new<Tucker::Matrix<scalar_t>>(nrows,ncols);
      nnz = nrows*ncols;
      dataptr = fact.U[d]->data();
      if(rank == 0) {
        for(size_t i=0; i<nnz; i++) {
          dataptr[i] = distribution(generator);
        }
      }
      TuckerMPI::MPI_Bcast_(dataptr,nnz,0,MPI_COMM_WORLD);
    }
    if(rank == 0) std::cout << "Factor matrices generated..." << std::endl;

    ////////////////////////////////////////////////////////
    // Construct the global tensor using a series of TTMs //
    ////////////////////////////////////////////////////////
    TuckerMPI::Tensor<scalar_t>* Y = fact.G;
    for(int d=0; d<nd; d++) {
      TuckerMPI::Tensor<scalar_t>* temp = TuckerMPI::ttm(Y, d, fact.U[d]);
      if(Y != fact.G) {
        Tucker::MemoryManager::safe_delete(Y);
      }
      Y = temp;
    }
    for(int d=0; d<nd; d++) {
      Tucker::MemoryManager::safe_delete(fact.U[d]);
    }
    if(rank == 0) std::cout << "Factor matrices generated..." << std::endl;

    if(addNoise){
      /////////////////////////////////////////////////////////////////////
      // Compute the norm of the global tensor                           //
      // \todo This could be more efficient; see Bader/Kolda for details //
      /////////////////////////////////////////////////////////////////////
      if(rank == 0) std::cout << "Computing the global tensor norm...\n";
      Tucker::Timer normTimer;
      normTimer.start();
      scalar_t normM = std::sqrt(Y->norm2());
      normTimer.stop();
      if(rank == 0) std::cout << "Time spent computing the tensor norm: " << normTimer.duration() << "s\n";

      ///////////////////////////////////////////////////////////////////
      // Compute the estimated norm of the noise matrix                //
      // The average of each element squared is the standard deviation //
      // squared, so this quantity should be sqrt(nnz * stdev^2)       //
      ///////////////////////////////////////////////////////////////////
      nnz = I_dims->prod();
      scalar_t normN = std::sqrt(nnz);

      ///////////////////
      // Compute alpha //
      ///////////////////
      scalar_t alpha = eps*normM/normN;

      //////////////////////////////////////////////////////////////////////
      // For each entry of the global tensor, add alpha*randn             //
      // Note that this is space-efficient, as we do not store the entire //
      // noise tensor                                                     //
      //////////////////////////////////////////////////////////////////////
      if(rank == 0) std::cout << "Adding noise...\n";
      Tucker::Timer noiseTimer;
      noiseTimer.start();
      dataptr = Y->getLocalTensor()->data();
      nnz = Y->getDistribution()->getLocalDims().prod();
      for(size_t i=0; i<nnz; i++) {
        dataptr[i] += alpha*distribution(generator);
      }
      noiseTimer.stop();
      if(rank == 0) std::cout << "Time spent adding noise: " << noiseTimer.duration() << "s\n";
    }
    int one = 1;
    int numLocalEntries = Y->getLocalNumEntries();
    Tucker::copy(&numLocalEntries, Y->getLocalTensor()->data(), &one, X->getLocalTensor()->data(), &one);
    Tucker::MemoryManager::safe_delete(Y);
  }
  else{
    ///////////////////////////////////////////////////////////////
    // Read tensor from files if not generating the input tensor //
    ///////////////////////////////////////////////////////////////
    Tucker::Timer readTimer;
    readTimer.start();
    TuckerMPI::readTensorBinary(input_file, *X);
    readTimer.stop();

    double localReadTime = readTimer.duration();
    double globalReadTime;
    MPI_Reduce(&localReadTime,&globalReadTime,1,MPI_DOUBLE,MPI_MAX,0,MPI_COMM_WORLD);
    if(rank == 0) {
      std::cout << "Time to read tensor: " << globalReadTime << " s" << std::endl;
    }
  }

  //////////////////////////////////////////
  // Read memory size of the input tensor //
  //////////////////////////////////////////
  if(rank == 0){
    size_t local_nnz = X->getLocalNumEntries();
    size_t global_nnz = X->getGlobalNumEntries();
    std::cout << "Local input tensor size: " << X->getLocalSize() << ", or ";
    Tucker::printBytes(local_nnz*sizeof(scalar_t));
    std::cout << "Global input tensor size: " << X->getGlobalSize() << ", or ";
    Tucker::printBytes(global_nnz*sizeof(scalar_t));
    std::cout <<std::endl;
  }

  for(int t=0; t<numTrials; t++){
    const TuckerMPI::TuckerTensor<scalar_t>* solution;
    Tucker::SizeArray** subRanksArr = Tucker::readSubRanks(subRanks_file);
    if(STHOSVD){
      if(rank==0){
        std::cout << "======================" << std::endl;
        std::cout << "trial " << t << ". Starting STHOSVD..." << std::endl;
        std::cout << "Input tensor size: " << X->getGlobalSize() << std::endl;
      }
      std::ostringstream ss;
        ss << output_directory << "/"  << "sthosvd_run" << t << ".csv";
      std::string timing_file = ss.str(); 
      int* modeOrder = Tucker::MemoryManager::safe_new_array<int>(nd);
      for(int i=0; i<nd; i++){
        modeOrder[i] = i;
      }
      MPI_Barrier(MPI_COMM_WORLD);
      solution = TuckerMPI::STHOSVD(X, R_dims, modeOrder, false); 
      solution->printTimers(timing_file); 
      if(t==0 && boolWriteResults) {
        ss.str("");
        ss.clear();
        ss << output_directory << "/sthosvd";
        std::string alg_output_directory = ss.str();  
        bool coreDistributed = true; // The core tensor computed with deterministic STHOSVD is always distributed
        TuckerMPI::writeResults(alg_output_directory, oversampling, coreDistributed, solution);
      }
      Tucker::MemoryManager::safe_delete(solution);
      if(rank==0) Tucker::MemoryManager::printCurrentMemUsage();
    }
    if(RSTHOSVD_dense_Gaussian){
      if(rank==0){
        std::cout << "======================" << std::endl;
        std::cout << "trial " << t << ". Starting RSTHOSVD with dense gaussian random matrices..." << std::endl;
        std::cout << "Input tensor size: " << X->getGlobalSize() << std::endl;
        Tucker::MemoryManager::printCurrentMemUsage();
      }
      std::ostringstream ss;
        ss << output_directory << "/"  << "rsthosvd_run" << t << ".csv";
      std::string timing_file = ss.str();
      MPI_Barrier(MPI_COMM_WORLD);
      solution = TuckerMPI::RSTHOSVD(X, 123, R_dims, oversampling, truncateCoreInParallel);
      solution->printTimersRsthosvd(timing_file);
      if(t==0 && boolWriteResults) {
        ss.str("");
        ss.clear();
        ss << output_directory << "/rsthosvd";
        std::string alg_output_directory = ss.str(); 
        TuckerMPI::writeResults(alg_output_directory, oversampling, truncateCoreInParallel, solution);
      }
      Tucker::MemoryManager::safe_delete(solution);
      if(rank==0) Tucker::MemoryManager::printCurrentMemUsage();
    }
    if(RSTHOSVD_kron_ttm){
      if(rank==0){
        std::cout << "======================" << std::endl;
        std::cout << "trial " << t << ". Starting RSTHOSVD with Kronecker structured random matrices and in sequence TTM..." << std::endl;
        std::cout << "Input tensor size: " << X->getGlobalSize() << std::endl;
      }
      std::ostringstream ss;
        ss << output_directory << "/"  << "rsthosvd_kron_ttm_run" << t << ".csv";
      std::string timing_file = ss.str();
      bool useDcttm = false;
      MPI_Barrier(MPI_COMM_WORLD);
      solution = TuckerMPI::RSTHOSVDKron(X, 123, R_dims, subRanksArr, useDcttm, useSrhtMatrices, truncateCoreInParallel);
      solution->printTimersRHOSVD(timing_file, useDcttm);
      if(t==0 && boolWriteResults) {
        ss.str("");
        ss.clear();
        ss << output_directory << "/rsthosvd_kron_ttm";
        std::string alg_output_directory = ss.str(); 
        TuckerMPI::writeResults(alg_output_directory, oversampling, truncateCoreInParallel, solution);
      }
      Tucker::MemoryManager::safe_delete(solution);
      if(rank==0) Tucker::MemoryManager::printCurrentMemUsage();
    }
    if(RSTHOSVD_kron_dcttm){
      if(rank==0){
        std::cout << "======================" << std::endl;
        std::cout << "trial " << t << ". Starting RSTHOSVD with Kronecker structured random matrices and all-at-once TTM..." << std::endl;
        std::cout << "Input tensor size: " << X->getGlobalSize() << std::endl;
      }
      std::ostringstream ss;
        ss << output_directory << "/"  << "rsthosvd_kron_dcttm_run" << t << ".csv";
      std::string timing_file = ss.str();
      bool useDcttm = true;
      MPI_Barrier(MPI_COMM_WORLD);
      solution = TuckerMPI::RSTHOSVDKron(X, 123, R_dims, subRanksArr, useDcttm, useSrhtMatrices, truncateCoreInParallel);
      solution->printTimersRHOSVD(timing_file, useDcttm);
      if(t==0 && boolWriteResults) {
        ss.str("");
        ss.clear();
        ss << output_directory << "/rsthosvd_kron_dcttm";
        std::string alg_output_directory = ss.str(); 
        TuckerMPI::writeResults(alg_output_directory, oversampling, truncateCoreInParallel, solution);
      }
      Tucker::MemoryManager::safe_delete(solution);
      if(rank==0) Tucker::MemoryManager::printCurrentMemUsage();
    }
    if(RHOSVD_ttm_noReuse){
      if(rank==0){
        std::cout << "======================" << std::endl;
        std::cout << "trial " << t << ". Starting RHOSVD with independent Kronecker-structured random matrices and in-sequence TTM..." << std::endl;
        std::cout << "Input tensor size: " << X->getGlobalSize() << std::endl;
      }
      std::ostringstream ss;
        ss << output_directory << "/"  << "rhosvd_ttm_noReuse_run" << t << ".csv";
      std::string timing_file = ss.str();
      bool useDcttm = false;
      MPI_Barrier(MPI_COMM_WORLD);
      solution = TuckerMPI::RHOSVD(X, 123, R_dims, subRanksArr, oversampling, useDcttm, useSrhtMatrices, truncateCoreInParallel);
      solution->printTimersRHOSVD(timing_file, useDcttm);
      if(t==0 && boolWriteResults) {
        ss.str("");
        ss.clear();
        ss << output_directory << "/rhosvd_ttm_noReuse";
        std::string alg_output_directory = ss.str(); 
        TuckerMPI::writeResults(alg_output_directory, oversampling, truncateCoreInParallel, solution);
      }
      Tucker::MemoryManager::safe_delete(solution);
      if(rank==0) Tucker::MemoryManager::printCurrentMemUsage();
    }
    if(RHOSVD_dcttm_noReuse){
      if(rank==0){
        std::cout << "======================" << std::endl;
        std::cout << "trial " << t << ". Starting RHOSVD with independent Kronecker-structured random matrices and all-at-once TTM..." << std::endl;
        std::cout << "Input tensor size: " << X->getGlobalSize() << std::endl;
      }
      std::ostringstream ss;
        ss << output_directory << "/"  << "rhosvd_dcttm_noReuse_run" << t << ".csv";
      std::string timing_file = ss.str();
      bool useDcttm = true;
      MPI_Barrier(MPI_COMM_WORLD);
      solution = TuckerMPI::RHOSVD(X, 123, R_dims, subRanksArr, oversampling, useDcttm, useSrhtMatrices, truncateCoreInParallel);
      solution->printTimersRHOSVD(timing_file, useDcttm);
      if(t==0 && boolWriteResults) {
        ss.str("");
        ss.clear();
        ss << output_directory << "/rhosvd_dcttm_noReuse";
        std::string alg_output_directory = ss.str(); 
        TuckerMPI::writeResults(alg_output_directory, oversampling, truncateCoreInParallel, solution);
      }
      Tucker::MemoryManager::safe_delete(solution);
      if(rank==0) Tucker::MemoryManager::printCurrentMemUsage();
    }
    if(RHOSVD_dcttm_reuse_noDimTree){
      if(rank==0){
        std::cout << "======================" << std::endl;
        std::cout << "trial " << t << ". Starting RHOSVD with reused Kronecker-structured random matrices and all-at-once TTM..." << std::endl;
        std::cout << "Input tensor size: " << X->getGlobalSize() << std::endl;
      }
      std::ostringstream ss;
        ss << output_directory << "/"  << "rhosvd_dcttm_noDimTree_run" << t << ".csv";
      std::string timing_file = ss.str();
      bool useDcttm = true;
      bool useDimTree = false;
      MPI_Barrier(MPI_COMM_WORLD);
      solution = TuckerMPI::RHOSVD(X, 123, R_dims, subRanks, oversampling, useDcttm, useDimTree, useSrhtMatrices, truncateCoreInParallel);
      solution->printTimersRHOSVD(timing_file, useDcttm);
      if(t==0 && boolWriteResults) {
        ss.str("");
        ss.clear();
        ss << output_directory << "/rhosvd_dcttm_noDimTree";
        std::string alg_output_directory = ss.str(); 
        TuckerMPI::writeResults(alg_output_directory, oversampling, truncateCoreInParallel, solution);
      }
      Tucker::MemoryManager::safe_delete(solution);
      if(rank==0) Tucker::MemoryManager::printCurrentMemUsage();
    }
    if(RHOSVD_dcttm_reuse_dimTree){
      if(rank==0){
        std::cout << "======================" << std::endl;
        std::cout << "trial " << t << ". Starting RHOSVD with reused Kronecker-structured random matrices and all-at-once TTM and dimension tree..." << std::endl;
        std::cout << "Input tensor size: " << X->getGlobalSize() << std::endl;
      }
      std::ostringstream ss;
        ss << output_directory << "/"  << "rhosvd_dcttm_dimTree_run" << t << ".csv";
      std::string timing_file = ss.str();
      bool nodeBreakdown = true;
      bool useDcttm = true;
      bool useDimTree = true;
      MPI_Barrier(MPI_COMM_WORLD);
      solution = TuckerMPI::RHOSVD(X, 123, R_dims, subRanks, oversampling, useDcttm, useDimTree, useSrhtMatrices, truncateCoreInParallel);
      solution->printTimersRHOSVDDimTree(timing_file, nodeBreakdown);
      if(t==0 && boolWriteResults) {
        ss.str("");
        ss.clear();
        ss << output_directory << "/rhosvd_dcttm_dimTree";
        std::string alg_output_directory = ss.str(); 
        TuckerMPI::writeResults(alg_output_directory, oversampling, truncateCoreInParallel, solution);
      }
      Tucker::MemoryManager::safe_delete(solution);
      if(rank==0) Tucker::MemoryManager::printCurrentMemUsage();
    }
  }
   
  if(computeNorm){
    scalar_t xNorm = X->norm2();
    if(truncateCoreInParallel || oversampling == 0 || deterministic){
      TuckerMPI::Tensor<scalar_t>* reconstX = solution->reconstructTensor();
      TuckerMPI::Tensor<scalar_t>* diffTensor = X->subtract(reconstX);
      scalar_t diffTensorNorm = diffTensor->norm2();
      if(rank==0){
        std::cout << "xNorm: " << std::sqrt(xNorm) << std::endl;
        std::cout << "relative error: " << std::sqrt(diffTensorNorm/xNorm) << std::endl;
      }
    }
    else{
      if(rank==0){
        scalar_t coreNorm = solution->gatheredCore->norm2();
        std::cout << "xNorm: " << std::sqrt(xNorm) << std::endl;
        std::cout << "norm difference: " << std::sqrt(xNorm) - std::sqrt(coreNorm) << std::endl;
      }
    }
  }

  MPI_Finalize();
  return EXIT_SUCCESS;
}
