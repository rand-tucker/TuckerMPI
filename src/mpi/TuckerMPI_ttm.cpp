/*
 * TuckerMPI_ttm.cpp
 *
 *  Created on: Nov 2, 2016
 *      Author: amklinv
 */
#include "TuckerMPI_TuckerTensor.hpp"
#include "TuckerMPI_ttm.hpp"
#include "Tucker.hpp"

namespace TuckerMPI
{



template <class scalar_t>
Tucker::Tensor<scalar_t>* dcttm(const Tensor<scalar_t>* X, int n, bool allGather, Tucker::Matrix<scalar_t>** U, 
  Tucker::Timer* mult_timer, 
  Tucker::Timer* all_reduce_timer, 
  Tucker::Timer* all_gather_timer, 
  Tucker::Timer* dist_timer,
  Tucker::Timer* mem_timer,
  Tucker::Timer* unpack_timer){
  int ndims = X->getNumDimensions(); 
  int nprocs;
  MPI_Comm_size(MPI_COMM_WORLD,&nprocs);
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  
  const Tensor<scalar_t>* Y = X;
  Tensor<scalar_t>* tempY;
  Tucker::Tensor<scalar_t>* R;
  Tucker::SizeArray newSize(ndims);
  int Pn = X->getDistribution()->getProcessorGrid()->getNumProcs(n, false);

  // Compute Y = Y \times_i U_i
  for(int i=0; i<ndims; i++){
    if(i!=n){
      //////////////////////////
      // Create the new tensor//
      //////////////////////////
      int unrows = (U[i])->nrows();
      for(int j=0; j<ndims; j++) {
        if(j == i)
          newSize[j] = unrows * Y->getDistribution()->getProcessorGrid()->getNumProcs(j, false);
        else
          newSize[j] = Y->getGlobalSize(j);
      }
      if(dist_timer) dist_timer->start();
      Distribution* dist = Tucker::MemoryManager::safe_new<Distribution>(newSize, X->getDistribution()->getProcessorGrid());
      MPI_Barrier(MPI_COMM_WORLD);//timing
      if(dist_timer) dist_timer->stop();

      tempY = Tucker::MemoryManager::safe_new<Tensor<scalar_t>>(dist);

      int Pi = X->getDistribution()->getProcessorGrid()->getNumProcs(i,false);
      if(Pi == 1){
        if(mult_timer) mult_timer->start();
        if(!Y->getDistribution()->ownNothing())
          Tucker::ttm(Y->getLocalTensor(), i, U[i], tempY->getLocalTensor());
        MPI_Barrier(MPI_COMM_WORLD);//for timing purposes, prevent leaking
        if(mult_timer) mult_timer->stop();
      }
      else{  
        const scalar_t* Uptr;
        Uptr = U[i]->data() + Y->getDistribution()->getMap(i, false)->getGlobalIndex(0)*unrows;
        int stride = unrows;
        if(mult_timer) mult_timer->start();
        // Compute the TTM
        if(Y->getDistribution()->ownNothing()) 
          tempY->getLocalTensor()->initialize();
        else 
          Tucker::ttm(Y->getLocalTensor(), i, Uptr, stride, tempY->getLocalTensor());
        MPI_Barrier(MPI_COMM_WORLD);//for timing purposes, prevent leaking
        if(mult_timer) mult_timer->stop();
      } 
      if(Y != X) Tucker::MemoryManager::safe_delete(Y);
      Y = tempY;
    }
    
  }
  // if(Pn == 1){
  //   //Only need a all-reduce
  //   R = Tucker::MemoryManager::safe_new<Tucker::Tensor<scalar_t>>(Y->getLocalTensor()->size());
  //   if(all_reduce_timer) all_reduce_timer->start();
  //   MPI_Allreduce_(Y->getLocalTensor()->data(), R->data(), Y->getLocalTensor()->getNumElements(), MPI_SUM, MPI_COMM_WORLD);
  //   MPI_Barrier(MPI_COMM_WORLD);//for timing purposes, prevent leaking
  //   if(all_reduce_timer) all_reduce_timer->stop();
  //   Tucker::MemoryManager::safe_delete(Y);
  // }//TODO: this if block is redundant 
  // else{
    
  //All-reduce among the row communicator of the mode n unfolding of the processor tensor.
  const MPI_Comm& rowComm = Y->getDistribution()->getProcessorGrid()->getRowComm(n, false);
  Tucker::Tensor<scalar_t>* tempT = Tucker::MemoryManager::safe_new<Tucker::Tensor<scalar_t>>(Y->getLocalTensor()->size());
  if(all_reduce_timer) all_reduce_timer->start();
  MPI_Allreduce_(Y->getLocalTensor()->data(), tempT->data(), Y->getLocalTensor()->getNumElements(), MPI_SUM, rowComm);
  MPI_Barrier(MPI_COMM_WORLD);//for timing purposes
  if(all_reduce_timer) all_reduce_timer->stop();
  //if we want the TTM result to be distributed along the each processor fiber of mode n, or if said processor fiber has only 1 processor, then an all-gather is not necessary.
  if(!allGather || Pn == 1){
    Tucker::MemoryManager::safe_delete(Y);
    R = tempT;
  }
  else{
    // if(overhead_timer) overhead_timer->start();
    //All-gather among the column communicator of the mode n unfolding of the processor tensor.
    const MPI_Comm& colComm = X->getDistribution()->getProcessorGrid()->getColComm(n, false);
    Tucker::SizeArray newSize(ndims);
    //This is the number of rows of the unfolding of the local tensor, it should be the same for every processor at this point.
    int standardSize = 1;
    for(int j=0; j<ndims; j++) {
      if(j == n) {
        newSize[j] = X->getGlobalSize(n);
      }
      else {
        newSize[j] = tempT->size(j);
        standardSize = standardSize * tempT->size(j);
      }
    }
    // if(mem_timer) mem_timer->start();
    Tucker::Tensor<scalar_t>* allGatherResult = Tucker::MemoryManager::safe_new<Tucker::Tensor<scalar_t>>(newSize);
    // if(mem_timer) mem_timer->stop();
    MPI_Comm_size(colComm, &nprocs);

    int* recvCount = Tucker::MemoryManager::safe_new_array<int>(nprocs);
    int* displacements = Tucker::MemoryManager::safe_new_array<int>(nprocs);
    const Map* yMap = Y->getDistribution()->getMap(n, false);
    for(int i=0; i<nprocs; i++){
      recvCount[i] = yMap->getNumElementsPerProc()->data()[i] * standardSize;
      displacements[i] = i==0? 0 : recvCount[i-1] + displacements[i-1];
    }
    
    if(all_gather_timer) all_gather_timer->start();
    MPI_Allgatherv_(tempT->data(), tempT->getNumElements(), allGatherResult->data(), recvCount, displacements, colComm);
    MPI_Barrier(MPI_COMM_WORLD);//for timing purposes, prevent leaking
    if(all_gather_timer) all_gather_timer->stop();
    Tucker::MemoryManager::safe_delete(tempT);
    //unpacking
    const Tucker::SizeArray& localSize = Y->getLocalSize();
    if(isUnpackForGathervNecessary(n, &localSize)){
      // if(mem_timer) mem_timer->start();
      R = Tucker::MemoryManager::safe_new<Tucker::Tensor<scalar_t>>(newSize);
      // if(mem_timer) mem_timer->stop();
      if(unpack_timer) unpack_timer->start();
      unpackForColCommGatherv(n, &localSize, Y->getDistribution()->getMap(n, false)->getNumElementsPerProc(), allGatherResult->data(), R->data());
      MPI_Barrier(MPI_COMM_WORLD);//timing
      if(unpack_timer) unpack_timer->stop();
      Tucker::MemoryManager::safe_delete(allGatherResult);
    }
    else{
      R = allGatherResult;
    }
    Tucker::MemoryManager::safe_delete(Y);
  }
  return R;
}

template <class scalar_t>
Tucker::Tensor<scalar_t>* dcttm(const Tensor<scalar_t>* X, Tucker::Matrix<scalar_t>** U, 
  Tucker::Timer* mult_timer, 
  Tucker::Timer* all_reduce_timer, 
  Tucker::Timer* newSize_timer,
  Tucker::Timer* dist_timer,
  Tucker::Timer* createMaps_timer,
  Tucker::Timer* mem_timer){

  int ndims = X->getNumDimensions(); 
  int nprocs;
  MPI_Comm_size(MPI_COMM_WORLD,&nprocs);
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  
  const Tensor<scalar_t>* Y = X;
  Tensor<scalar_t>* tempY;
  Tucker::Tensor<scalar_t>* R;
  Tucker::SizeArray newSize(ndims);

  // Compute Y = Y \times_i U_i
  for(int i=0; i<ndims; i++){
    //////////////////////////
    // Create the new tensor//
    //////////////////////////
    if(newSize_timer) newSize_timer->start();
    int unrows = (U[i])->nrows();
    for(int j=0; j<ndims; j++) {
      if(j == i)
        newSize[j] = unrows * Y->getDistribution()->getProcessorGrid()->getNumProcs(j, false);
      else
        newSize[j] = Y->getGlobalSize(j);
    }
    MPI_Barrier(MPI_COMM_WORLD);//timing
    if(newSize_timer) newSize_timer->stop();
    if(dist_timer) dist_timer->start();
    Distribution* dist = Tucker::MemoryManager::safe_new<Distribution>(newSize, X->getDistribution()->getProcessorGrid(), createMaps_timer);
    MPI_Barrier(MPI_COMM_WORLD);//timing
    if(dist_timer) dist_timer->stop();

    tempY = Tucker::MemoryManager::safe_new<Tensor<scalar_t>>(dist);

    int Pi = X->getDistribution()->getProcessorGrid()->getNumProcs(i,false);
    if(Pi == 1){
      if(mult_timer) mult_timer->start();
      if(!Y->getDistribution()->ownNothing())
        Tucker::ttm(Y->getLocalTensor(), i, U[i], tempY->getLocalTensor());
      MPI_Barrier(MPI_COMM_WORLD);//for timing purposes, prevent leaking
      if(mult_timer) mult_timer->stop();
    }
    else{  
      const scalar_t* Uptr;
      Uptr = U[i]->data() + Y->getDistribution()->getMap(i, false)->getGlobalIndex(0)*unrows;
      int stride = unrows;
      if(mult_timer) mult_timer->start();
      // Compute the TTM
      if(Y->getDistribution()->ownNothing()) 
        tempY->getLocalTensor()->initialize();
      else 
        Tucker::ttm(Y->getLocalTensor(), i, Uptr, stride, tempY->getLocalTensor());
      MPI_Barrier(MPI_COMM_WORLD);//for timing purposes, prevent leaking
      if(mult_timer) mult_timer->stop();
    } 
    if(mem_timer) mem_timer->start();
    if(Y != X) Tucker::MemoryManager::safe_delete(Y);
    Y = tempY;
    MPI_Barrier(MPI_COMM_WORLD);//for timing purposes, prevent leaking
    if(mem_timer) mem_timer->stop();
  }
  //Only need a all-reduce
  R = Tucker::MemoryManager::safe_new<Tucker::Tensor<scalar_t>>(Y->getLocalTensor()->size());
  if(all_reduce_timer) all_reduce_timer->start();
  MPI_Allreduce_(Y->getLocalTensor()->data(), R->data(), Y->getLocalTensor()->getNumElements(), MPI_SUM, MPI_COMM_WORLD);
  MPI_Barrier(MPI_COMM_WORLD); //for timing purposes, prevent leaking
  if(all_reduce_timer) all_reduce_timer->stop();
  Tucker::MemoryManager::safe_delete(Y);
  return R;
}


template <class scalar_t>
Tensor<scalar_t>* ttm(const Tensor<scalar_t>* X, const int n,
    const Tucker::Matrix<scalar_t>* const U, bool Utransp,
    Tucker::Timer* mult_timer, 
    Tucker::Timer* pack_timer,
    Tucker::Timer* reduce_scatter_timer, 
    Tucker::Timer* reduce_timer,
    Tucker::Timer* dist_timer,
    Tucker::Timer* mem_timer,// using this to time the createMap function in distribution constructor...
    size_t nnz_limit)
{  
  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  // Compute the number of rows for the resulting "matrix"
  int nrows;
  if(Utransp)
    nrows = U->ncols();
  else
    nrows = U->nrows();
  // Get the size of the new tensor
  int ndims = X->getNumDimensions();
  Tucker::SizeArray newSize(ndims);
  for(int i=0; i<ndims; i++) {
    if(i == n) {
      newSize[i] = nrows;
    }
    else {
      newSize[i] = X->getGlobalSize(i);
    }
  }
  // Create a distribution object for it
  if(dist_timer) dist_timer->start();
  Distribution* dist = Tucker::MemoryManager::safe_new<Distribution>(newSize,
          X->getDistribution()->getProcessorGrid(), mem_timer);
  // Distribution* dist = Tucker::MemoryManager::safe_new<Distribution>(newSize,
  //         X->getDistribution()->getProcessorGrid()->size());
  MPI_Barrier(MPI_COMM_WORLD);//timing
  if(dist_timer) dist_timer->stop();
  // if(mem_timer) mem_timer->start();
  // Create the new tensor
  Tensor<scalar_t>* Y = Tucker::MemoryManager::safe_new<Tensor<scalar_t>>(dist);
  // if(mem_timer) mem_timer->stop();
  // Get the local part of the tensor
  const Tucker::Tensor<scalar_t>* localX = X->getLocalTensor();
  Tucker::Tensor<scalar_t>* localY = Y->getLocalTensor();

  // Determine whether there are multiple MPI processes along this dimension
  int Pn = X->getDistribution()->getProcessorGrid()->getNumProcs(n,false);
  if(Pn == 1)
  {
    if(!X->getDistribution()->ownNothing()) {
      // Compute the TTM
      if(mult_timer) mult_timer->start();
      Tucker::ttm(localX, n, U, localY, Utransp);
      MPI_Barrier(MPI_COMM_WORLD);//for timing purposes, prevent leaking
      if(mult_timer) mult_timer->stop();
      // if(rank == 0) std::cout<< "computed mode " << n << ", TTM time: "<< mult_timer->duration() << std::endl; 
    }
  }
  else
  {
    // Get the local communicator
    const MPI_Comm& comm = X->getDistribution()->getProcessorGrid()->getColComm(n,false);

    // Determine whether we must block the result
    // If the temporary storage is bigger than the tensor, we block instead
    // int K = nrows;
    // int Jn = Utransp ? U->nrows() : U->ncols();

    int uGlobalRows = Y->getGlobalSize(n);
    int uGlobalCols = X->getGlobalSize(n);

    const Map* xMap = X->getDistribution()->getMap(n,false);
    const Map* yMap = Y->getDistribution()->getMap(n,false);

    const scalar_t* Uptr;
    assert(U->getNumElements() > 0);
    if(Utransp)
      Uptr = U->data() + xMap->getGlobalIndex(0);
    else
      Uptr = U->data() + xMap->getGlobalIndex(0)*uGlobalRows;

    int stride;
    if(Utransp)
      stride = uGlobalCols;
    else
      stride = uGlobalRows;

    // We can do the TTM either by doing a single reduce_scatter, or a
    // series of reductions.
    // Reduce_scatter tends to be faster, so we try to use it if the
    // memory requirements are not prohibitive.

    // Compute the nnz of the largest tensor piece being stored by any process
    size_t max_lcl_nnz_x = 1;
    for(int i=0; i<ndims; i++) {
      max_lcl_nnz_x *= X->getDistribution()->getMap(i,false)->getMaxNumEntries();
    }

    // Compute the nnz required for the reduce_scatter
    size_t nnz_reduce_scatter = 1;
    for(int i=0; i<ndims; i++) {
      if(i == n)
        nnz_reduce_scatter *= Y->getGlobalSize(n);
      else
        nnz_reduce_scatter *= X->getDistribution()->getMap(i,false)->getMaxNumEntries();
    }

    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    // If the required memory is small, we can do a single reduce_scatter
    if(nnz_reduce_scatter <= std::max(max_lcl_nnz_x,nnz_limit)) {
      // Compute the TTM
      Tucker::Tensor<scalar_t>* localResult;
      if(mult_timer) mult_timer->start();
      if(X->getDistribution()->ownNothing()) {
        // std::cout << "rank: " << rank<< " owns nothing" << std::endl;
        Tucker::SizeArray sz(ndims);
        for(int i=0; i<ndims; i++) {
          sz[i] = X->getLocalSize(i);
        }
        sz[n] = Y->getGlobalSize(n);
        localResult = Tucker::MemoryManager::safe_new<Tucker::Tensor<scalar_t>>(sz);
        localResult->initialize();
      }
      else {
        localResult = Tucker::ttm(localX, n, Uptr, uGlobalRows, stride, Utransp);
      }
      MPI_Barrier(MPI_COMM_WORLD);//for timing purposes, prevent leaking
      if(mult_timer) mult_timer->stop();
      // Pack the data
      if(pack_timer) pack_timer->start();
      packForTTM(localResult,n,yMap);
      MPI_Barrier(MPI_COMM_WORLD);//timing
      if(pack_timer) pack_timer->stop();

      // Perform a reduce-scatter
      const scalar_t* sendBuf;
      if(localResult->getNumElements() > 0)
        sendBuf = localResult->data();
      else
        sendBuf = 0;
      scalar_t* recvBuf;
      if(localY->getNumElements() > 0)
        recvBuf = localY->data();
      else
        recvBuf = 0;
      int nprocs;
      MPI_Comm_size(comm,&nprocs);
      int* recvCounts = Tucker::MemoryManager::safe_new_array<int>(nprocs);
      size_t multiplier = Y->getLocalSize().prod(0,n-1,1)*Y->getLocalSize().prod(n+1,ndims-1,1);
      for(int i=0; i<nprocs; i++) {
        size_t temp = multiplier*(yMap->getNumEntries(i));
        assert(temp <= std::numeric_limits<int>::max());
        recvCounts[i] = (int)temp;
      }
      if(reduce_scatter_timer) reduce_scatter_timer->start();
      MPI_Reduce_scatter_(sendBuf, recvBuf, recvCounts, MPI_SUM, comm);
      MPI_Barrier(MPI_COMM_WORLD);//for timing purposes, prevent leaking
      if(reduce_scatter_timer) reduce_scatter_timer->stop();
      Tucker::MemoryManager::safe_delete_array(recvCounts,nprocs);
      Tucker::MemoryManager::safe_delete(localResult);
    } 
    else {
      for(int root=0; root<Pn; root++) {
        int uLocalRows = yMap->getNumEntries(root);
        if(uLocalRows == 0) {
          continue;
        }

        // Compute the local TTM
        Tucker::Tensor<scalar_t>* localResult;
        if(mult_timer) mult_timer->start();
        if(X->getDistribution()->ownNothing()) {
          Tucker::SizeArray sz(ndims);
          for(int i=0; i<ndims; i++) {
            sz[i] = X->getLocalSize(i);
          }
          sz[n] = uLocalRows;
          localResult = Tucker::MemoryManager::safe_new<Tucker::Tensor<scalar_t>>(sz);
          localResult->initialize();
        }
        else {
          localResult = Tucker::ttm(localX, n, Uptr, uLocalRows, stride, Utransp);
        }
        MPI_Barrier(MPI_COMM_WORLD);//for timing purposes, prevent leaking
        if(mult_timer) mult_timer->stop();

        // Combine the local results with a reduce operation
        const scalar_t* sendBuf;
        if(localResult->getNumElements() > 0)
          sendBuf = localResult->data();
        else
          sendBuf = 0;
        scalar_t* recvBuf;
        if(localY->getNumElements() > 0)
          recvBuf = localY->data();
        else
          recvBuf = 0;
        size_t count = localResult->getNumElements();
        assert(count <= std::numeric_limits<int>::max());

        if(count > 0) {
          if(reduce_timer) reduce_timer->start();
          MPI_Reduce_(sendBuf, recvBuf, (int)count, MPI_SUM, root, comm);
          MPI_Barrier(MPI_COMM_WORLD);//for timing purposes, prevent leaking
          if(reduce_timer) reduce_timer->stop();
        }
        // Free memory
        Tucker::MemoryManager::safe_delete(localResult);
        // Increment the data pointer
        if(Utransp)
          Uptr += (uLocalRows*stride);
        else
          Uptr += uLocalRows;
      } // end for i = 0 .. Pn-1
    } // end if K >= Jn/Pn
  } // end if Pn != 1

  // Return the result
  return Y;
}

// Explicit instantiations to build static library for both single and double precision
template Tensor<float>* ttm(const Tensor<float>* X, const int n,
    const Tucker::Matrix<float>* const U, bool Utransp,
    Tucker::Timer* mult_timer, 
    Tucker::Timer* pack_timer,
    Tucker::Timer* reduce_scatter_timer, 
    Tucker::Timer* reduce_timer, 
    Tucker::Timer* dist_timer, 
    Tucker::Timer* mem_timer, 
    size_t nnz_limit);
template Tensor<double>* ttm(const Tensor<double>* X, const int n,
    const Tucker::Matrix<double>* const U, bool Utransp,
    Tucker::Timer* mult_timer, 
    Tucker::Timer* pack_timer,
    Tucker::Timer* reduce_scatter_timer, 
    Tucker::Timer* reduce_timer,
    Tucker::Timer* dist_timer, 
    Tucker::Timer* mem_timer, 
    size_t nnz_limit);

// Explicit instantiations to build static library for both single and double precision
template Tucker::Tensor<float>* dcttm(const Tensor<float>* X, int n, bool allGather, Tucker::Matrix<float>** U, 
  Tucker::Timer* mult_timer, 
  Tucker::Timer* all_reduce_timer, 
  Tucker::Timer* all_gather_timer, 
  Tucker::Timer* dist_timer,
  Tucker::Timer* mem_timer,
  Tucker::Timer* unpack_timer);
template Tucker::Tensor<double>* dcttm(const Tensor<double>* X, int n, bool allGather, Tucker::Matrix<double>** U, 
  Tucker::Timer* mult_timer, 
  Tucker::Timer* all_reduce_timer, 
  Tucker::Timer* all_gather_timer, 
  Tucker::Timer* dist_timer,
  Tucker::Timer* mem_timer,
  Tucker::Timer* unpack_timer);

  template Tucker::Tensor<float>* dcttm(const Tensor<float>* X, Tucker::Matrix<float>** U, 
  Tucker::Timer* mult_timer, 
  Tucker::Timer* all_reduce_timer, 
  Tucker::Timer* newSize_timer,
  Tucker::Timer* dist_timer,
  Tucker::Timer* createMaps_timer,
  Tucker::Timer* mem_timer);

  template Tucker::Tensor<double>* dcttm(const Tensor<double>* X, Tucker::Matrix<double>** U, 
  Tucker::Timer* mult_timer, 
  Tucker::Timer* all_reduce_timer, 
  Tucker::Timer* newSize_timer,
  Tucker::Timer* dist_timer,
  Tucker::Timer* createMaps_timer,
  Tucker::Timer* mem_timer);
} // end namespace TuckerMPI


