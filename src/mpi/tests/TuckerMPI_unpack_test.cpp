#include "TuckerMPI.hpp"
#include <iostream>
#include <cstdlib>

int main(int argc, char* argv[])
{
  std::string filename = "input_files/dcttm_input.mpi";
  Tucker::SizeArray* size =
      Tucker::MemoryManager::safe_new<Tucker::SizeArray>(4);
  (*size)[0] = 4;
  (*size)[1] = 8;
  (*size)[2] = 6;
  (*size)[3] = 5;

  Tucker::Tensor* tensor = Tucker::MemoryManager::safe_new<Tucker::Tensor>(size);
  Tucker::importTensorBinary(tensor, filename.c_str());

    // Create the processor grid
  Tucker::SizeArray* nprocsPerDim =
      Tucker::MemoryManager::safe_new<Tucker::SizeArray>(4);
  (*nprocsPerDim)[0] = 1;
  (*nprocsPerDim)[1] = 2;
  (*nprocsPerDim)[2] = 2;
  (*nprocsPerDim)[3] = 3;

  // Create the distribution object
  TuckerMPI::Distribution* dist =
        Tucker::MemoryManager::safe_new<TuckerMPI::Distribution>(*size,*nprocsPerDim);

  // Create the tensor
  TuckerMPI::Tensor* t =
      Tucker::MemoryManager::safe_new<TuckerMPI::Tensor>(dist);

  TuckerMPI::importTensorBinary(filename.c_str(),t);
  

}