#include "TuckerMPI.hpp"
#include "TuckerMPI_Util.hpp"
#include <limits>
#include <cmath>
#include <map>
#include <iostream>
#include <iomanip>
#include <cstdlib>

template <class scalar_t>
bool checkEqual(const Tucker::Tensor<scalar_t>* t1, const Tucker::Tensor<scalar_t>* t2)
{
  if(t1->N() != t2->N()){
    std::cout << "t1 and t2 doesn't have the same dimensions" << std::endl;
    return false;
  }
  for(int i=0; i<t1->N(); i++){
    if(t1->size(i) != t2->size(i)){
      std::cout << "t1 dim[" << i <<"] size: " << t1->size(i) << ". t2 dim[" << i <<"] size: " << t1->size(i) << std::endl;
      return false;
    }
  }
  for(int i=0; i<t1->getNumElements(); i++){
    if(t1->data()[i] != t2->data()[i]){
      std::cout << "t1->data()[" << i << "]: " << t1->data()[i] << ". t2->data()[" << i <<"]: " << t2->data()[i] << std::endl;
      return false;
    }
  }
  return true;
}

int main(int argc, char* argv[])
{
  #ifdef TEST_SINGLE
    typedef float scalar_t;
    std::string filename = "input_files/lq_data_single.mpi"; 
  #else
    typedef double scalar_t;
    std::string filename = "input_files/lq_data.mpi";
  #endif


  MPI_Init(&argc, &argv);

  int rank;
  int np;
  MPI_Comm_size(MPI_COMM_WORLD, &np);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);

  int ndims = 4;
  std::map<int, int*> pointerMap;
  std::map<int, int> sizeMap;
  long d = (sizeof(int)*ndims);
  int processorGridLayouts_12[160] = {1,1,1,12
                                        ,1,1,12,1
                                        ,1,12,1,1
                                        ,12,1,1,1
                                        ,1,1,2,6
                                        ,1,1,6,2
                                        ,1,2,1,6
                                        ,1,2,6,1
                                        ,1,6,1,2
                                        ,1,6,2,1
                                        ,2,1,1,6
                                        ,2,1,6,1
                                        ,2,6,1,1
                                        ,6,1,1,2
                                        ,6,1,2,1
                                        ,6,2,1,1
                                        ,1,1,3,4
                                        ,1,1,4,3
                                        ,1,3,1,4
                                        ,1,3,4,1
                                        ,1,4,1,3
                                        ,1,4,3,1
                                        ,3,1,1,4
                                        ,3,1,4,1
                                        ,3,4,1,1
                                        ,4,1,1,3
                                        ,4,1,3,1
                                        ,4,3,1,1
                                        ,1,2,2,3
                                        ,1,2,3,2
                                        ,1,3,2,2
                                        ,2,1,2,3
                                        ,2,1,3,2
                                        ,2,2,1,3
                                        ,2,2,3,1
                                        ,2,3,1,2
                                        ,2,3,2,1
                                        ,3,1,2,2
                                        ,3,2,1,2
                                        ,3,2,2,1};
  pointerMap[12] = &processorGridLayouts_12[0]; sizeMap[12] = sizeof(processorGridLayouts_12)/d;
  int processorGridLayouts_11[16] ={1,1,1,11
                                      ,1,1,11,1
                                      ,1,11,1,1
                                      ,11,1,1,1};
  pointerMap[11] = &processorGridLayouts_11[0]; sizeMap[11] = sizeof(processorGridLayouts_11)/d;
  int processorGridLayouts_10[64] = {1,1,2,5
                                        ,1,1,5,2
                                        ,1,2,1,5
                                        ,1,2,5,1
                                        ,1,5,1,2
                                        ,1,5,2,1
                                        ,2,1,1,5
                                        ,2,1,5,1
                                        ,2,5,1,1
                                        ,5,1,1,2
                                        ,5,1,2,1
                                        ,5,2,1,1
                                        ,1,1,1,10
                                        ,1,1,10,1
                                        ,1,10,1,1
                                        ,10,1,1,1};
  pointerMap[10] = &processorGridLayouts_10[0]; sizeMap[10] = sizeof(processorGridLayouts_10)/d;
  int processorGridLayouts_9[40] = {1,1,3,3
                                      ,1,3,1,3
                                      ,1,3,3,1
                                      ,3,1,1,3
                                      ,3,1,3,1
                                      ,3,3,1,1
                                      ,1,1,1,9
                                      ,1,1,9,1
                                      ,1,9,1,1
                                      ,9,1,1,1};
  pointerMap[9] = &processorGridLayouts_9[0]; sizeMap[9] = sizeof(processorGridLayouts_9)/d;
  int processorGridLayouts_8[80] = {1,1,1,8
                                      ,1,1,8,1
                                      ,1,8,1,1
                                      ,8,1,1,1
                                      ,1,2,2,2
                                      ,2,1,2,2
                                      ,2,2,1,2
                                      ,2,2,2,1
                                      ,1,1,2,4
                                      ,1,1,4,2
                                      ,1,2,1,4
                                      ,1,2,4,1
                                      ,1,4,1,2
                                      ,1,4,2,1
                                      ,2,1,1,4
                                      ,2,1,4,1
                                      ,2,4,1,1
                                      ,4,1,1,2
                                      ,4,1,2,1
                                      ,4,2,1,1};                                      
  pointerMap[8] = &processorGridLayouts_8[0]; sizeMap[8] = sizeof(processorGridLayouts_8)/d;
  int processorGridLayouts_7[16] = {1,1,1,7
                                      ,1,1,7,1
                                      ,1,7,1,1
                                      ,7,1,1,1};
  pointerMap[7] = &processorGridLayouts_7[0]; sizeMap[7] = sizeof(processorGridLayouts_7)/d;
  int processorGridLayouts_6[64] = {1,1,1,6
                                      ,1,1,6,1
                                      ,1,6,1,1
                                      ,6,1,1,1
                                      ,1,1,2,3
                                      ,1,1,3,2
                                      ,1,2,1,3
                                      ,1,2,3,1
                                      ,1,3,1,2
                                      ,1,3,2,1
                                      ,2,1,1,3
                                      ,2,1,3,1
                                      ,2,3,1,1
                                      ,3,1,1,2
                                      ,3,1,2,1
                                      ,3,2,1,1};
  pointerMap[6] = &processorGridLayouts_6[0]; sizeMap[6] = sizeof(processorGridLayouts_6)/d;
  int processorGridLayouts_5[16] = {1,1,1,5
                                      ,1,1,5,1
                                      ,1,5,1,1
                                      ,5,1,1,1};
  pointerMap[5] = &processorGridLayouts_5[0]; sizeMap[5] = sizeof(processorGridLayouts_5)/d;
  int processorGridLayouts_4[40] = {1,1,1,4,
                                    1,1,4,1,
                                    1,4,1,1,
                                    4,1,1,1,
                                    1,1,2,2,
                                    1,2,1,2,
                                    2,1,1,2,
                                    1,2,2,1,
                                    2,1,2,1,
                                    2,2,1,1};                                    
  pointerMap[4] = &processorGridLayouts_4[0]; sizeMap[4] = sizeof(processorGridLayouts_4)/d;
  int processorGridLayouts_3[16] = {1,1,1,3
                                      ,1,1,3,1
                                      ,1,3,1,1
                                      ,3,1,1,1};
  pointerMap[3] = &processorGridLayouts_3[0]; sizeMap[3] = sizeof(processorGridLayouts_3)/d;
  int processorGridLayouts_2[16] = {1,1,1,2,
                                   1,1,2,1,
                                   1,2,1,1,
                                   2,1,1,1};
  pointerMap[2] = &processorGridLayouts_2[0]; sizeMap[2] = sizeof(processorGridLayouts_2)/d;
  int processorGridLayouts_1[4] = {1,1,1,1};
  pointerMap[1] = &processorGridLayouts_1[0]; sizeMap[1] = sizeof(processorGridLayouts_1)/d;
  int nPossibleProcGrid = sizeMap[np];
  int* processorGridLayouts = pointerMap[np];

  Tucker::SizeArray sz(ndims);
  (sz)[0] = 12; (sz)[1] = 12; (sz)[2] = 12; (sz)[3] = 12;

  Tucker::Tensor<scalar_t>* T = Tucker::MemoryManager::safe_new<Tucker::Tensor<scalar_t>>(sz);
  Tucker::importTensorBinary(T, filename.c_str());
  TuckerMPI::Tensor<scalar_t>* tensor;
  for(int t=0; t<nPossibleProcGrid; t++){
    Tucker::SizeArray* nprocsPerDim = Tucker::MemoryManager::safe_new<Tucker::SizeArray>(ndims);
    for(int i=0; i<ndims; i++){
      (*nprocsPerDim)[i] = *(processorGridLayouts+t*ndims+i);
      if(rank==0) std::cout << (*nprocsPerDim)[i] << ", ";
    }
    TuckerMPI::Distribution* dist = Tucker::MemoryManager::safe_new<TuckerMPI::Distribution>(sz,*nprocsPerDim);
    tensor = Tucker::MemoryManager::safe_new<TuckerMPI::Tensor<scalar_t>>(dist);
    TuckerMPI::importTensorBinary(filename.c_str(),tensor);
    const Tucker::Tensor<scalar_t>* gT = TuckerMPI::allGatherLocalTensor(tensor);
    if(!checkEqual(T, gT)) return EXIT_FAILURE;
  }
  MPI_Finalize();
  return EXIT_SUCCESS;
}