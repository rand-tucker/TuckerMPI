#include "TuckerMPI.hpp"
#include <iostream>
#include <cstdlib>

int main(int argc, char* argv[])
{
  // specify precision
  #ifdef TEST_SINGLE
    typedef float scalar_t;
    std::string filename = "input_files/dcttm_input_single.mpi"; 
  #else
    typedef double scalar_t;
    std::string filename = "input_files/dcttm_input.mpi";
  #endif

  scalar_t tol =  100* std::numeric_limits<scalar_t>::epsilon();
  bool approxEqual;

  // Initialize MPI
  MPI_Init(&argc,&argv);

  // Create the SizeArray
  Tucker::SizeArray* size =
      Tucker::MemoryManager::safe_new<Tucker::SizeArray>(4);
  (*size)[0] = 4;
  (*size)[1] = 8;
  (*size)[2] = 6;
  (*size)[3] = 5;

  // Create the processor grid
  Tucker::SizeArray* nprocsPerDim =
      Tucker::MemoryManager::safe_new<Tucker::SizeArray>(4);
  (*nprocsPerDim)[0] = 1;
  (*nprocsPerDim)[1] = 2;
  (*nprocsPerDim)[2] = 2;
  (*nprocsPerDim)[3] = 3;

  // Create the distribution object
  TuckerMPI::Distribution* dist =
        Tucker::MemoryManager::safe_new<TuckerMPI::Distribution>(*size,*nprocsPerDim);

  // Create the tensor
  TuckerMPI::Tensor<scalar_t>* t =
      Tucker::MemoryManager::safe_new<TuckerMPI::Tensor<scalar_t>>(dist);

  // Read the values from a file
  std::string inputFilename = filename;
  TuckerMPI::importTensorBinary(inputFilename.c_str(),t);

	Tucker::Tensor<scalar_t>** trueResults = Tucker::MemoryManager::safe_new_array<Tucker::Tensor<scalar_t>*>(4);
	Tucker::SizeArray* ansSize = Tucker::MemoryManager::safe_new<Tucker::SizeArray>(4);
  std::string baseFileName = "input_files/dcttm_ans";
	std::string extension = ".txt";
	for(int i=0; i<4; i++){
		std::string s = std::to_string(i);
		std::string fileName = baseFileName + s + extension;
    trueResults[i] = Tucker::importTensor<scalar_t>(fileName.c_str());;
  }

  ///////////////////////////////////////////////////////////////////
  // First test: mode 0, no transpose
  ///////////////////////////////////////////////////////////////////

  // Create matrices to multiply
  Tucker::Matrix<scalar_t>* mat0 =
      Tucker::MemoryManager::safe_new<Tucker::Matrix<scalar_t>>(3,4);
  scalar_t* data = mat0->data();
  data[0]=1;	data[3]=3;	data[6]=3;	data[9]=3;
  data[1]=4;	data[4]=3;	data[7]=2;	data[10]=3;
  data[2]=3;	data[5]=3;	data[8]=3;	data[11]=4;

  Tucker::Matrix<scalar_t>* mat1 =
      Tucker::MemoryManager::safe_new<Tucker::Matrix<scalar_t>>(3,8);
  data = mat1->data();
  data[0]=2;	data[3]=1;	data[6]=3;	data[9]=3;	data[12]=4;	data[15]=2;	data[18]=3;	data[21]=1;
  data[1]=3;	data[4]=3;	data[7]=5;	data[10]=1;	data[13]=2;	data[16]=5;	data[19]=1;	data[22]=4;
  data[2]=3;	data[5]=2;	data[8]=2;	data[11]=2;	data[14]=2;	data[17]=1;	data[20]=3;	data[23]=2;

  Tucker::Matrix<scalar_t>* mat2 =
      Tucker::MemoryManager::safe_new<Tucker::Matrix<scalar_t>>(3,6);
  data = mat2->data();
  data[0]=4;	data[3]=2;	data[6]=2;	data[9]=1;	data[12]=3;	data[15]=2;
  data[1]=2;	data[4]=1;	data[7]=4;	data[10]=2;	data[13]=1;	data[16]=1;
  data[2]=1;	data[5]=3;	data[8]=3;	data[11]=3;	data[14]=4;	data[17]=5;


  Tucker::Matrix<scalar_t>* mat3 =
      Tucker::MemoryManager::safe_new<Tucker::Matrix<scalar_t>>(3,5);
  data = mat3->data();
  data[0]=1;	data[3]=3;	data[6]=4;	data[9]=2;	data[12]=2;
  data[1]=5;	data[4]=1;	data[7]=5;	data[10]=3;	data[13]=1;
  data[2]=4;	data[5]=2;	data[8]=4;	data[11]=5;	data[14]=3;

  Tucker::Matrix<scalar_t>** matrices = Tucker::MemoryManager::safe_new_array<Tucker::Matrix<scalar_t>*>(4);
  matrices[0] = mat0; matrices[1] = mat1; matrices[2] = mat2; matrices[3] = mat3;

  int rank;
  MPI_Comm_rank(MPI_COMM_WORLD,&rank);
  for(int i=0; i<4; i++){
    Tucker::Tensor<scalar_t>* result = TuckerMPI::dcttm(t,i,true,matrices);
    approxEqual = Tucker::isApproxEqual(result,trueResults[i], tol);
    if(!approxEqual) {
      if(rank == 0) std::cout << "not equal!" << std::endl;
      MPI_Finalize();
      return EXIT_FAILURE;
    } 
  }

  MPI_Finalize();
  return EXIT_SUCCESS;
}

