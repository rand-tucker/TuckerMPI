/*
 * TuckerMPI_ttm.hpp
 *
 *  Created on: Nov 2, 2016
 *      Author: amklinv
 */

#ifndef MPI_TUCKERMPI_TTM_HPP_
#define MPI_TUCKERMPI_TTM_HPP_

#include<cmath>
#include "Tucker.hpp"
#include "TuckerMPI_Util.hpp"
namespace TuckerMPI
{
/** \brief Delayed communication TTM of a d-way tensor, X, with d-1 matrices. 
 *
 * \param X d-way Input tensor
 * \param n Do a series of dcttm on each mode of X with one of U, except for mode n
 * \param U Pointer to d-1 Matrix pointer 
 */
template <class scalar_t>
Tucker::Tensor<scalar_t>* dcttm(const Tensor<scalar_t>* X, int n, bool allGather,
    Tucker::Matrix<scalar_t>** U, 
    Tucker::Timer* mult_timer=0,
    Tucker::Timer* all_reduce_timer=0, 
    Tucker::Timer* all_gather_timer=0, 
    Tucker::Timer* dist_timer=0, 
    Tucker::Timer* mem_timer=0,
    Tucker::Timer* unpack_timer=0);

/** \brief Delayed communication TTM of a d-way tensor, X, with d matrices. 
 *
 * \param X d-way Input tensor
 * \param U Pointer to d Matrix pointer 
 */
template <class scalar_t>
Tucker::Tensor<scalar_t>* dcttm(const Tensor<scalar_t>* X, Tucker::Matrix<scalar_t>** U, 
    Tucker::Timer* mult_timer=0,
    Tucker::Timer* all_reduce_timer=0, 
    Tucker::Timer* newSize_timer=0,
    Tucker::Timer* dist_timer=0, 
    Tucker::Timer* createMaps_timer=0,
    Tucker::Timer* mem_timer=0);

/** \brief Parallel tensor times matrix computation. Note in this implementation the processor grid 
 *  of the new tensor is inherited from the input tensor. As soon as you delete the old tensor, the 
 *  new tensor is without a processor grid. TODO: one fix is to make inheriting processor grid depend
 *  on a parameter and default to not inheriting.
 *
 * \param X A parallel tensor
 * \param n The dimension for the tensor unfolding
 * \param U A sequential matrix
 * \param Utransp Whether to compute X * U^T or X * U
 * \param mult_timer Timer for the local multiplication
 * \param pack_timer Timer for packing the data
 * \param reduce_scatter_timer Timer for the reduce-scatter
 * \param reduce_timer Timer for the reduction
 */
template <class scalar_t>
Tensor<scalar_t>* ttm(const Tensor<scalar_t>* X, const int n,
    const Tucker::Matrix<scalar_t>* const U, bool Utransp=false,
    Tucker::Timer* mult_timer=0, 
    Tucker::Timer* pack_timer=0,
    Tucker::Timer* reduce_scatter_timer=0, 
    Tucker::Timer* reduce_timer=0, 
    Tucker::Timer* dist_timer=0, 
    Tucker::Timer* mem_timer=0, 
    size_t nnz_limit=0);


} // end namespace TuckerMPI

#endif /* MPI_TUCKERMPI_TTM_HPP_ */
