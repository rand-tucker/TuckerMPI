#include <cmath>
#include <limits>
#include "Tucker.hpp"

template <class scalar_t>
bool checkEqual(const scalar_t* arr1, const scalar_t* arr2, int nrows, int ncols)
{
    int ind = 0;
    for(int c=0; c<ncols; c++) {
      for(int r=0; r<nrows; r++) {
        //std::cout << "matching:  arr1["<< r << ", " << c<< "]: " << arr1[r+c*nrows] << ", arr2[" << ind << "]: " << arr2[ind] << std::endl;
        if(std::abs(std::abs(arr1[r+c*nrows]) - std::abs(arr2[ind])) > 100 * std::numeric_limits<scalar_t>::epsilon()) {
          std::cout << arr1[r+c*nrows] << ", " << arr2[r+c] << std::endl;
          return false;
        }
        ind++;
      }
    }
  return true;
}

int main(){

// Specify precision
#ifdef TEST_SINGLE
  typedef float scalar_t; 
#else
  typedef double scalar_t;
#endif

  Tucker::Tensor<scalar_t>* T = Tucker::importTensor<scalar_t>("input_files/10x10x10x10_sketch.txt");
  Tucker::Matrix<scalar_t>* omegaTranspose = Tucker::importMatrix<scalar_t>("input_files/omegaTranspose_sketch.txt");
  Tucker::Matrix<scalar_t>* R0    = Tucker::importMatrix<scalar_t>("input_files/10x5_sketch_mode0.txt");
  Tucker::Matrix<scalar_t>* R1    = Tucker::importMatrix<scalar_t>("input_files/10x5_sketch_mode1.txt");
  Tucker::Matrix<scalar_t>* R2    = Tucker::importMatrix<scalar_t>("input_files/10x5_sketch_mode2.txt");
  Tucker::Matrix<scalar_t>* R3    = Tucker::importMatrix<scalar_t>("input_files/10x5_sketch_mode3.txt");

  Tucker::Matrix<scalar_t>* computedR0 = Tucker::computeSketch(T, 0, omegaTranspose);
  // std::cout << computedR0->prettyPrint() << std::endl;
  Tucker::Matrix<scalar_t>* computedR1 = Tucker::computeSketch(T, 1, omegaTranspose);
  Tucker::Matrix<scalar_t>* computedR2 = Tucker::computeSketch(T, 2, omegaTranspose);
  Tucker::Matrix<scalar_t>* computedR3 = Tucker::computeSketch(T, 3, omegaTranspose);

  bool isEqual0 = checkEqual(R0->data(), computedR0->data(), computedR0->nrows(), computedR0->ncols());
  bool isEqual1 = checkEqual(R1->data(), computedR1->data(), computedR1->nrows(), computedR1->ncols());
  bool isEqual2 = checkEqual(R2->data(), computedR2->data(), computedR2->nrows(), computedR2->ncols());
  bool isEqual3 = checkEqual(R3->data(), computedR3->data(), computedR3->nrows(), computedR3->ncols());
  if(!isEqual0 || !isEqual1 || !isEqual2 || !isEqual3){
     return EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}