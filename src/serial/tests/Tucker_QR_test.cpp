#include <cmath>
#include <limits>
#include "Tucker.hpp"

// template <class scalar_t>
// bool checkEqual(const scalar_t* arr1, const scalar_t* arr2, int nrows, int ncols)
// {
//   int ind = 0;
//   for(int c=0; c<ncols; c++) {
//     for(int r=0; r<nrows; r++) {
//       //std::cout << "matching:  arr1["<< r << ", " << c<< "]: " << arr1[r+c*nrows] << ", arr2[" << ind << "]: " << arr2[ind] << std::endl;
//       if(std::abs(std::abs(arr1[r+c*nrows]) - std::abs(arr2[ind])) > 100 * std::numeric_limits<scalar_t>::epsilon()) {
//         std::cout << arr1[r+c*nrows] << ", " << arr2[r+c] << std::endl;
//         return EXIT_FAILURE;
//       }
//       ind++;
//     }
//   }
//   return true;
// }

template <class scalar_t>
bool checkEqual(const scalar_t* arr1, const scalar_t* arr2, int nElements)
{
  for(int r=0; r<nElements; r++) {
    
    if(std::abs(arr1[r] - arr2[r]) > 100 * std::numeric_limits<scalar_t>::epsilon()) {
      std::cout << "matching:  arr1["<< r << "]: " << arr1[r] << ", arr2[" << r << "]: " << arr2[r] << std::endl;
      std::cout << "error: "<< std::abs(arr1[r] - arr2[r]) << ". tolerance: " << std::numeric_limits<scalar_t>::epsilon() <<std::endl;
      return EXIT_FAILURE;
    }
  }
  return true;
}

int main(){
  // Specify precision
  #ifdef TEST_SINGLE
    typedef float scalar_t; 
  #else
    typedef double scalar_t;
  #endif

  Tucker::SizeArray* size = Tucker::MemoryManager::safe_new<Tucker::SizeArray>(4);
  int sizes[4] = {3,2,3,2};
  for(int i=0; i<4; i++)
    (*size)[i] = sizes[i];
  Tucker::Tensor<scalar_t>* Y = Tucker::MemoryManager::safe_new<Tucker::Tensor<scalar_t>>(*size);
  int YContent[36] = {1,4,4,2,1,3,3,2,1,4,3,2,5,2,4,6,3,1,6,3,1,5,2,4,4,3,2,3,2,1,2,1,3,1,4,4};
  for(int i=0; i<36; i++){
    Y->data()[i] = YContent[i];
  }

  scalar_t** trueQs = Tucker::MemoryManager::safe_new_array<scalar_t*>(4);

  scalar_t Y0QContent[9] = {  
    -0.174077655955698,-0.696310623822791,-0.696310623822791, 0.711286759159019,-0.577920491816704, 0.400098802026948,-0.681005224606999,-0.425628265379374, 0.595879571531124
  };
  scalar_t Y1QContent[4] = {
    -0.4472, -0.8944, -0.8944,  0.4472
  };
  scalar_t Y2QContent[9] = {
    -0.1690, -0.5071, -0.8452,  0.9670,  0.0806, -0.2417,  0.1907, -0.8581,  0.4767
  };
  scalar_t Y3QContent[4] = {  
    -0.164398987305357,-0.986393923832144,-0.986393923832144, 0.164398987305357
  };
  trueQs[0] = Y0QContent;
  trueQs[1] = Y1QContent;
  trueQs[2] = Y2QContent;
  trueQs[3] = Y3QContent;

  // for(int i=0; i<4; i++){
  //   Tucker::Matrix<scalar_t>* Qc = Tucker::computeQR(Y, i);
  //   checkEqual(Qc->data(), trueQs[i], Qc->getNumElements());
  // }

  for(int i=0; i<1; i++){
    Tucker::Matrix<scalar_t>* Qc = Tucker::computeQRF(Y, i);
    // if(checkEqual(Qc->data(), trueQs[i], Qc->getNumElements())){
    //   return EXIT_FAILURE;
    // }
  }
  
  return EXIT_SUCCESS;
}